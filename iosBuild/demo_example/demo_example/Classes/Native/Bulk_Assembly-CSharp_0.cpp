﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// Block
struct Block_t1429612866;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// System.Action`1<Block>
struct Action_1_t1602080461;
// System.Delegate
struct Delegate_t1188392813;
// System.Action
struct Action_t1264377477;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.Material
struct Material_t340375123;
// System.String
struct String_t;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Texture
struct Texture_t3661962703;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// Block/<AnimateMove>c__Iterator0
struct U3CAnimateMoveU3Ec__Iterator0_t300133182;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// UnityEngine.Texture2D[0...,0...]
struct Texture2DU5B0___U2C0___U5D_t149664597;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;
// Puzzle
struct Puzzle_t2606799644;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Camera
struct Camera_t4157153871;
// System.Collections.Generic.Queue`1<Block>
struct Queue_1_t1275872360;
// System.Collections.Generic.Queue`1<System.Object>
struct Queue_1_t2926365658;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// Block[]
struct BlockU5BU5D_t2917470071;
// System.Void
struct Void_t1185182177;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// Block[0...,0...]
struct BlockU5B0___U2C0___U5D_t2917470072;

extern RuntimeClass* Action_1_t1602080461_il2cpp_TypeInfo_var;
extern const uint32_t Block_add_OnBlockPressed_m238060131_MetadataUsageId;
extern const uint32_t Block_remove_OnBlockPressed_m3588265933_MetadataUsageId;
extern RuntimeClass* Action_t1264377477_il2cpp_TypeInfo_var;
extern const uint32_t Block_add_OnFinishedMoving_m2464650543_MetadataUsageId;
extern const uint32_t Block_remove_OnFinishedMoving_m1922473105_MetadataUsageId;
extern const RuntimeMethod* Component_GetComponent_TisMeshRenderer_t587009260_m2899624428_RuntimeMethod_var;
extern const RuntimeMethod* Resources_Load_TisMaterial_t340375123_m2720476655_RuntimeMethod_var;
extern String_t* _stringLiteral424788472;
extern const uint32_t Block_Init_m3829536269_MetadataUsageId;
extern const RuntimeMethod* Action_1_Invoke_m1342042192_RuntimeMethod_var;
extern const uint32_t Block_OnMouseDown_m2987425322_MetadataUsageId;
extern RuntimeClass* U3CAnimateMoveU3Ec__Iterator0_t300133182_il2cpp_TypeInfo_var;
extern const uint32_t Block_AnimateMove_m3207370942_MetadataUsageId;
extern RuntimeClass* Vector2Int_t3469998543_il2cpp_TypeInfo_var;
extern const uint32_t Block_IsAtStartingCoord_m1016848544_MetadataUsageId;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern const uint32_t U3CAnimateMoveU3Ec__Iterator0_MoveNext_m2844285960_MetadataUsageId;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern const RuntimeMethod* U3CAnimateMoveU3Ec__Iterator0_Reset_m1788849239_RuntimeMethod_var;
extern const uint32_t U3CAnimateMoveU3Ec__Iterator0_Reset_m1788849239_MetadataUsageId;
extern RuntimeClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern RuntimeClass* Texture2DU5B0___U2C0___U5D_t149664597_il2cpp_TypeInfo_var;
extern RuntimeClass* Texture2D_t3840446185_il2cpp_TypeInfo_var;
extern const uint32_t ImageSlicer_GetSlices_m2783497285_MetadataUsageId;
extern RuntimeClass* BlockU5B0___U2C0___U5D_t2917470072_il2cpp_TypeInfo_var;
extern RuntimeClass* Queue_1_t1275872360_il2cpp_TypeInfo_var;
extern const RuntimeMethod* GameObject_AddComponent_TisBlock_t1429612866_m2794020862_RuntimeMethod_var;
extern const RuntimeMethod* Puzzle_PlayerMoveBlockInput_m630074849_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m1790430673_RuntimeMethod_var;
extern const RuntimeMethod* Puzzle_OnBlockFinishedMoving_m4170377730_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1__ctor_m2416854755_RuntimeMethod_var;
extern const uint32_t Puzzle_CreatePuzzle_m3554627909_MetadataUsageId;
extern const RuntimeMethod* Queue_1_Enqueue_m2019291069_RuntimeMethod_var;
extern const uint32_t Puzzle_PlayerMoveBlockInput_m630074849_MetadataUsageId;
extern const RuntimeMethod* Queue_1_Dequeue_m1430048205_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_get_Count_m896737555_RuntimeMethod_var;
extern const uint32_t Puzzle_MakeNextPlayerMove_m3707527570_MetadataUsageId;
extern const uint32_t Puzzle_MoveBlock_m1974819708_MetadataUsageId;
extern RuntimeClass* Vector2IntU5BU5D_t2878452246_il2cpp_TypeInfo_var;
extern const uint32_t Puzzle_MakeNextShuffleMove_m3103018245_MetadataUsageId;

struct Texture2DU5B0___U2C0___U5D_t149664597;
struct ColorU5BU5D_t941916413;
struct BlockU5B0___U2C0___U5D_t2917470072;
struct Vector2IntU5BU5D_t2878452246;


#ifndef U3CMODULEU3E_T692745536_H
#define U3CMODULEU3E_T692745536_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745536 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745536_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t4013366056* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t4013366056* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t4013366056* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t, ___inner_exception_1)); }
	inline Exception_t * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T_H
#ifndef IMAGESLICER_T2169353122_H
#define IMAGESLICER_T2169353122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ImageSlicer
struct  ImageSlicer_t2169353122  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IMAGESLICER_T2169353122_H
#ifndef QUEUE_1_T1275872360_H
#define QUEUE_1_T1275872360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1<Block>
struct  Queue_1_t1275872360  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	BlockU5BU5D_t2917470071* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_4;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_t1275872360, ____array_0)); }
	inline BlockU5BU5D_t2917470071* get__array_0() const { return ____array_0; }
	inline BlockU5BU5D_t2917470071** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(BlockU5BU5D_t2917470071* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_t1275872360, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__tail_2() { return static_cast<int32_t>(offsetof(Queue_1_t1275872360, ____tail_2)); }
	inline int32_t get__tail_2() const { return ____tail_2; }
	inline int32_t* get_address_of__tail_2() { return &____tail_2; }
	inline void set__tail_2(int32_t value)
	{
		____tail_2 = value;
	}

	inline static int32_t get_offset_of__size_3() { return static_cast<int32_t>(offsetof(Queue_1_t1275872360, ____size_3)); }
	inline int32_t get__size_3() const { return ____size_3; }
	inline int32_t* get_address_of__size_3() { return &____size_3; }
	inline void set__size_3(int32_t value)
	{
		____size_3 = value;
	}

	inline static int32_t get_offset_of__version_4() { return static_cast<int32_t>(offsetof(Queue_1_t1275872360, ____version_4)); }
	inline int32_t get__version_4() const { return ____version_4; }
	inline int32_t* get_address_of__version_4() { return &____version_4; }
	inline void set__version_4(int32_t value)
	{
		____version_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUE_1_T1275872360_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_0;
	// System.Single UnityEngine.Vector3::y
	float ___y_1;
	// System.Single UnityEngine.Vector3::z
	float ___z_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_3;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_12;

public:
	inline static int32_t get_offset_of_zeroVector_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_3)); }
	inline Vector3_t3722313464  get_zeroVector_3() const { return ___zeroVector_3; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_3() { return &___zeroVector_3; }
	inline void set_zeroVector_3(Vector3_t3722313464  value)
	{
		___zeroVector_3 = value;
	}

	inline static int32_t get_offset_of_oneVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_4)); }
	inline Vector3_t3722313464  get_oneVector_4() const { return ___oneVector_4; }
	inline Vector3_t3722313464 * get_address_of_oneVector_4() { return &___oneVector_4; }
	inline void set_oneVector_4(Vector3_t3722313464  value)
	{
		___oneVector_4 = value;
	}

	inline static int32_t get_offset_of_upVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_5)); }
	inline Vector3_t3722313464  get_upVector_5() const { return ___upVector_5; }
	inline Vector3_t3722313464 * get_address_of_upVector_5() { return &___upVector_5; }
	inline void set_upVector_5(Vector3_t3722313464  value)
	{
		___upVector_5 = value;
	}

	inline static int32_t get_offset_of_downVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_6)); }
	inline Vector3_t3722313464  get_downVector_6() const { return ___downVector_6; }
	inline Vector3_t3722313464 * get_address_of_downVector_6() { return &___downVector_6; }
	inline void set_downVector_6(Vector3_t3722313464  value)
	{
		___downVector_6 = value;
	}

	inline static int32_t get_offset_of_leftVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_7)); }
	inline Vector3_t3722313464  get_leftVector_7() const { return ___leftVector_7; }
	inline Vector3_t3722313464 * get_address_of_leftVector_7() { return &___leftVector_7; }
	inline void set_leftVector_7(Vector3_t3722313464  value)
	{
		___leftVector_7 = value;
	}

	inline static int32_t get_offset_of_rightVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_8)); }
	inline Vector3_t3722313464  get_rightVector_8() const { return ___rightVector_8; }
	inline Vector3_t3722313464 * get_address_of_rightVector_8() { return &___rightVector_8; }
	inline void set_rightVector_8(Vector3_t3722313464  value)
	{
		___rightVector_8 = value;
	}

	inline static int32_t get_offset_of_forwardVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_9)); }
	inline Vector3_t3722313464  get_forwardVector_9() const { return ___forwardVector_9; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_9() { return &___forwardVector_9; }
	inline void set_forwardVector_9(Vector3_t3722313464  value)
	{
		___forwardVector_9 = value;
	}

	inline static int32_t get_offset_of_backVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_10)); }
	inline Vector3_t3722313464  get_backVector_10() const { return ___backVector_10; }
	inline Vector3_t3722313464 * get_address_of_backVector_10() { return &___backVector_10; }
	inline void set_backVector_10(Vector3_t3722313464  value)
	{
		___backVector_10 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_11)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_11() const { return ___positiveInfinityVector_11; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_11() { return &___positiveInfinityVector_11; }
	inline void set_positiveInfinityVector_11(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_11 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_12)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_12() const { return ___negativeInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_12() { return &___negativeInfinityVector_12; }
	inline void set_negativeInfinityVector_12(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR2INT_T3469998543_H
#define VECTOR2INT_T3469998543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2Int
struct  Vector2Int_t3469998543 
{
public:
	// System.Int32 UnityEngine.Vector2Int::m_X
	int32_t ___m_X_0;
	// System.Int32 UnityEngine.Vector2Int::m_Y
	int32_t ___m_Y_1;

public:
	inline static int32_t get_offset_of_m_X_0() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543, ___m_X_0)); }
	inline int32_t get_m_X_0() const { return ___m_X_0; }
	inline int32_t* get_address_of_m_X_0() { return &___m_X_0; }
	inline void set_m_X_0(int32_t value)
	{
		___m_X_0 = value;
	}

	inline static int32_t get_offset_of_m_Y_1() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543, ___m_Y_1)); }
	inline int32_t get_m_Y_1() const { return ___m_Y_1; }
	inline int32_t* get_address_of_m_Y_1() { return &___m_Y_1; }
	inline void set_m_Y_1(int32_t value)
	{
		___m_Y_1 = value;
	}
};

struct Vector2Int_t3469998543_StaticFields
{
public:
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Zero
	Vector2Int_t3469998543  ___s_Zero_2;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_One
	Vector2Int_t3469998543  ___s_One_3;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Up
	Vector2Int_t3469998543  ___s_Up_4;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Down
	Vector2Int_t3469998543  ___s_Down_5;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Left
	Vector2Int_t3469998543  ___s_Left_6;
	// UnityEngine.Vector2Int UnityEngine.Vector2Int::s_Right
	Vector2Int_t3469998543  ___s_Right_7;

public:
	inline static int32_t get_offset_of_s_Zero_2() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Zero_2)); }
	inline Vector2Int_t3469998543  get_s_Zero_2() const { return ___s_Zero_2; }
	inline Vector2Int_t3469998543 * get_address_of_s_Zero_2() { return &___s_Zero_2; }
	inline void set_s_Zero_2(Vector2Int_t3469998543  value)
	{
		___s_Zero_2 = value;
	}

	inline static int32_t get_offset_of_s_One_3() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_One_3)); }
	inline Vector2Int_t3469998543  get_s_One_3() const { return ___s_One_3; }
	inline Vector2Int_t3469998543 * get_address_of_s_One_3() { return &___s_One_3; }
	inline void set_s_One_3(Vector2Int_t3469998543  value)
	{
		___s_One_3 = value;
	}

	inline static int32_t get_offset_of_s_Up_4() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Up_4)); }
	inline Vector2Int_t3469998543  get_s_Up_4() const { return ___s_Up_4; }
	inline Vector2Int_t3469998543 * get_address_of_s_Up_4() { return &___s_Up_4; }
	inline void set_s_Up_4(Vector2Int_t3469998543  value)
	{
		___s_Up_4 = value;
	}

	inline static int32_t get_offset_of_s_Down_5() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Down_5)); }
	inline Vector2Int_t3469998543  get_s_Down_5() const { return ___s_Down_5; }
	inline Vector2Int_t3469998543 * get_address_of_s_Down_5() { return &___s_Down_5; }
	inline void set_s_Down_5(Vector2Int_t3469998543  value)
	{
		___s_Down_5 = value;
	}

	inline static int32_t get_offset_of_s_Left_6() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Left_6)); }
	inline Vector2Int_t3469998543  get_s_Left_6() const { return ___s_Left_6; }
	inline Vector2Int_t3469998543 * get_address_of_s_Left_6() { return &___s_Left_6; }
	inline void set_s_Left_6(Vector2Int_t3469998543  value)
	{
		___s_Left_6 = value;
	}

	inline static int32_t get_offset_of_s_Right_7() { return static_cast<int32_t>(offsetof(Vector2Int_t3469998543_StaticFields, ___s_Right_7)); }
	inline Vector2Int_t3469998543  get_s_Right_7() const { return ___s_Right_7; }
	inline Vector2Int_t3469998543 * get_address_of_s_Right_7() { return &___s_Right_7; }
	inline void set_s_Right_7(Vector2Int_t3469998543  value)
	{
		___s_Right_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2INT_T3469998543_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef TEXTUREWRAPMODE_T584250749_H
#define TEXTUREWRAPMODE_T584250749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t584250749 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureWrapMode_t584250749, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T584250749_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef PUZZLESTATE_T2105210100_H
#define PUZZLESTATE_T2105210100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Puzzle/PuzzleState
struct  PuzzleState_t2105210100 
{
public:
	// System.Int32 Puzzle/PuzzleState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PuzzleState_t2105210100, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLESTATE_T2105210100_H
#ifndef PRIMITIVETYPE_T3468579401_H
#define PRIMITIVETYPE_T3468579401_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PrimitiveType
struct  PrimitiveType_t3468579401 
{
public:
	// System.Int32 UnityEngine.PrimitiveType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PrimitiveType_t3468579401, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPE_T3468579401_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef U3CANIMATEMOVEU3EC__ITERATOR0_T300133182_H
#define U3CANIMATEMOVEU3EC__ITERATOR0_T300133182_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Block/<AnimateMove>c__Iterator0
struct  U3CAnimateMoveU3Ec__Iterator0_t300133182  : public RuntimeObject
{
public:
	// UnityEngine.Vector2 Block/<AnimateMove>c__Iterator0::<initialPos>__0
	Vector2_t2156229523  ___U3CinitialPosU3E__0_0;
	// System.Single Block/<AnimateMove>c__Iterator0::<percent>__0
	float ___U3CpercentU3E__0_1;
	// System.Single Block/<AnimateMove>c__Iterator0::duration
	float ___duration_2;
	// UnityEngine.Vector2 Block/<AnimateMove>c__Iterator0::target
	Vector2_t2156229523  ___target_3;
	// Block Block/<AnimateMove>c__Iterator0::$this
	Block_t1429612866 * ___U24this_4;
	// System.Object Block/<AnimateMove>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Block/<AnimateMove>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 Block/<AnimateMove>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CinitialPosU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAnimateMoveU3Ec__Iterator0_t300133182, ___U3CinitialPosU3E__0_0)); }
	inline Vector2_t2156229523  get_U3CinitialPosU3E__0_0() const { return ___U3CinitialPosU3E__0_0; }
	inline Vector2_t2156229523 * get_address_of_U3CinitialPosU3E__0_0() { return &___U3CinitialPosU3E__0_0; }
	inline void set_U3CinitialPosU3E__0_0(Vector2_t2156229523  value)
	{
		___U3CinitialPosU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CpercentU3E__0_1() { return static_cast<int32_t>(offsetof(U3CAnimateMoveU3Ec__Iterator0_t300133182, ___U3CpercentU3E__0_1)); }
	inline float get_U3CpercentU3E__0_1() const { return ___U3CpercentU3E__0_1; }
	inline float* get_address_of_U3CpercentU3E__0_1() { return &___U3CpercentU3E__0_1; }
	inline void set_U3CpercentU3E__0_1(float value)
	{
		___U3CpercentU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(U3CAnimateMoveU3Ec__Iterator0_t300133182, ___duration_2)); }
	inline float get_duration_2() const { return ___duration_2; }
	inline float* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(float value)
	{
		___duration_2 = value;
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(U3CAnimateMoveU3Ec__Iterator0_t300133182, ___target_3)); }
	inline Vector2_t2156229523  get_target_3() const { return ___target_3; }
	inline Vector2_t2156229523 * get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(Vector2_t2156229523  value)
	{
		___target_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CAnimateMoveU3Ec__Iterator0_t300133182, ___U24this_4)); }
	inline Block_t1429612866 * get_U24this_4() const { return ___U24this_4; }
	inline Block_t1429612866 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(Block_t1429612866 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CAnimateMoveU3Ec__Iterator0_t300133182, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CAnimateMoveU3Ec__Iterator0_t300133182, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CAnimateMoveU3Ec__Iterator0_t300133182, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEMOVEU3EC__ITERATOR0_T300133182_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MATERIAL_T340375123_H
#define MATERIAL_T340375123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t340375123  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T340375123_H
#ifndef GAMEOBJECT_T1113636619_H
#define GAMEOBJECT_T1113636619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t1113636619  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T1113636619_H
#ifndef TRANSFORM_T3600365921_H
#define TRANSFORM_T3600365921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t3600365921  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T3600365921_H
#ifndef ACTION_1_T1602080461_H
#define ACTION_1_T1602080461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<Block>
struct  Action_1_t1602080461  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T1602080461_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef ACTION_T1264377477_H
#define ACTION_T1264377477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action
struct  Action_t1264377477  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T1264377477_H
#ifndef RENDERER_T2627027031_H
#define RENDERER_T2627027031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Renderer
struct  Renderer_t2627027031  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERER_T2627027031_H
#ifndef TEXTURE2D_T3840446185_H
#define TEXTURE2D_T3840446185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3840446185  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3840446185_H
#ifndef CAMERA_T4157153871_H
#define CAMERA_T4157153871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Camera
struct  Camera_t4157153871  : public Behaviour_t1437897464
{
public:

public:
};

struct Camera_t4157153871_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t190067161 * ___onPreCull_2;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t190067161 * ___onPreRender_3;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t190067161 * ___onPostRender_4;

public:
	inline static int32_t get_offset_of_onPreCull_2() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreCull_2)); }
	inline CameraCallback_t190067161 * get_onPreCull_2() const { return ___onPreCull_2; }
	inline CameraCallback_t190067161 ** get_address_of_onPreCull_2() { return &___onPreCull_2; }
	inline void set_onPreCull_2(CameraCallback_t190067161 * value)
	{
		___onPreCull_2 = value;
		Il2CppCodeGenWriteBarrier((&___onPreCull_2), value);
	}

	inline static int32_t get_offset_of_onPreRender_3() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPreRender_3)); }
	inline CameraCallback_t190067161 * get_onPreRender_3() const { return ___onPreRender_3; }
	inline CameraCallback_t190067161 ** get_address_of_onPreRender_3() { return &___onPreRender_3; }
	inline void set_onPreRender_3(CameraCallback_t190067161 * value)
	{
		___onPreRender_3 = value;
		Il2CppCodeGenWriteBarrier((&___onPreRender_3), value);
	}

	inline static int32_t get_offset_of_onPostRender_4() { return static_cast<int32_t>(offsetof(Camera_t4157153871_StaticFields, ___onPostRender_4)); }
	inline CameraCallback_t190067161 * get_onPostRender_4() const { return ___onPostRender_4; }
	inline CameraCallback_t190067161 ** get_address_of_onPostRender_4() { return &___onPostRender_4; }
	inline void set_onPostRender_4(CameraCallback_t190067161 * value)
	{
		___onPostRender_4 = value;
		Il2CppCodeGenWriteBarrier((&___onPostRender_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERA_T4157153871_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef MESHRENDERER_T587009260_H
#define MESHRENDERER_T587009260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MeshRenderer
struct  MeshRenderer_t587009260  : public Renderer_t2627027031
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHRENDERER_T587009260_H
#ifndef PUZZLE_T2606799644_H
#define PUZZLE_T2606799644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Puzzle
struct  Puzzle_t2606799644  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D Puzzle::image
	Texture2D_t3840446185 * ___image_2;
	// System.Int32 Puzzle::blocksPerLine
	int32_t ___blocksPerLine_3;
	// System.Int32 Puzzle::shuffleLength
	int32_t ___shuffleLength_4;
	// System.Single Puzzle::defaultMoveDuration
	float ___defaultMoveDuration_5;
	// System.Single Puzzle::shuffleMoveDuration
	float ___shuffleMoveDuration_6;
	// Puzzle/PuzzleState Puzzle::state
	int32_t ___state_7;
	// Block Puzzle::emptyBlock
	Block_t1429612866 * ___emptyBlock_8;
	// Block[0...,0...] Puzzle::blocks
	BlockU5B0___U2C0___U5D_t2917470072* ___blocks_9;
	// System.Collections.Generic.Queue`1<Block> Puzzle::inputs
	Queue_1_t1275872360 * ___inputs_10;
	// System.Boolean Puzzle::blockIsMoving
	bool ___blockIsMoving_11;
	// System.Int32 Puzzle::shuffleMovesRemaining
	int32_t ___shuffleMovesRemaining_12;
	// UnityEngine.Vector2Int Puzzle::prevShuffleOffset
	Vector2Int_t3469998543  ___prevShuffleOffset_13;
	// System.Boolean Puzzle::shufle
	bool ___shufle_14;

public:
	inline static int32_t get_offset_of_image_2() { return static_cast<int32_t>(offsetof(Puzzle_t2606799644, ___image_2)); }
	inline Texture2D_t3840446185 * get_image_2() const { return ___image_2; }
	inline Texture2D_t3840446185 ** get_address_of_image_2() { return &___image_2; }
	inline void set_image_2(Texture2D_t3840446185 * value)
	{
		___image_2 = value;
		Il2CppCodeGenWriteBarrier((&___image_2), value);
	}

	inline static int32_t get_offset_of_blocksPerLine_3() { return static_cast<int32_t>(offsetof(Puzzle_t2606799644, ___blocksPerLine_3)); }
	inline int32_t get_blocksPerLine_3() const { return ___blocksPerLine_3; }
	inline int32_t* get_address_of_blocksPerLine_3() { return &___blocksPerLine_3; }
	inline void set_blocksPerLine_3(int32_t value)
	{
		___blocksPerLine_3 = value;
	}

	inline static int32_t get_offset_of_shuffleLength_4() { return static_cast<int32_t>(offsetof(Puzzle_t2606799644, ___shuffleLength_4)); }
	inline int32_t get_shuffleLength_4() const { return ___shuffleLength_4; }
	inline int32_t* get_address_of_shuffleLength_4() { return &___shuffleLength_4; }
	inline void set_shuffleLength_4(int32_t value)
	{
		___shuffleLength_4 = value;
	}

	inline static int32_t get_offset_of_defaultMoveDuration_5() { return static_cast<int32_t>(offsetof(Puzzle_t2606799644, ___defaultMoveDuration_5)); }
	inline float get_defaultMoveDuration_5() const { return ___defaultMoveDuration_5; }
	inline float* get_address_of_defaultMoveDuration_5() { return &___defaultMoveDuration_5; }
	inline void set_defaultMoveDuration_5(float value)
	{
		___defaultMoveDuration_5 = value;
	}

	inline static int32_t get_offset_of_shuffleMoveDuration_6() { return static_cast<int32_t>(offsetof(Puzzle_t2606799644, ___shuffleMoveDuration_6)); }
	inline float get_shuffleMoveDuration_6() const { return ___shuffleMoveDuration_6; }
	inline float* get_address_of_shuffleMoveDuration_6() { return &___shuffleMoveDuration_6; }
	inline void set_shuffleMoveDuration_6(float value)
	{
		___shuffleMoveDuration_6 = value;
	}

	inline static int32_t get_offset_of_state_7() { return static_cast<int32_t>(offsetof(Puzzle_t2606799644, ___state_7)); }
	inline int32_t get_state_7() const { return ___state_7; }
	inline int32_t* get_address_of_state_7() { return &___state_7; }
	inline void set_state_7(int32_t value)
	{
		___state_7 = value;
	}

	inline static int32_t get_offset_of_emptyBlock_8() { return static_cast<int32_t>(offsetof(Puzzle_t2606799644, ___emptyBlock_8)); }
	inline Block_t1429612866 * get_emptyBlock_8() const { return ___emptyBlock_8; }
	inline Block_t1429612866 ** get_address_of_emptyBlock_8() { return &___emptyBlock_8; }
	inline void set_emptyBlock_8(Block_t1429612866 * value)
	{
		___emptyBlock_8 = value;
		Il2CppCodeGenWriteBarrier((&___emptyBlock_8), value);
	}

	inline static int32_t get_offset_of_blocks_9() { return static_cast<int32_t>(offsetof(Puzzle_t2606799644, ___blocks_9)); }
	inline BlockU5B0___U2C0___U5D_t2917470072* get_blocks_9() const { return ___blocks_9; }
	inline BlockU5B0___U2C0___U5D_t2917470072** get_address_of_blocks_9() { return &___blocks_9; }
	inline void set_blocks_9(BlockU5B0___U2C0___U5D_t2917470072* value)
	{
		___blocks_9 = value;
		Il2CppCodeGenWriteBarrier((&___blocks_9), value);
	}

	inline static int32_t get_offset_of_inputs_10() { return static_cast<int32_t>(offsetof(Puzzle_t2606799644, ___inputs_10)); }
	inline Queue_1_t1275872360 * get_inputs_10() const { return ___inputs_10; }
	inline Queue_1_t1275872360 ** get_address_of_inputs_10() { return &___inputs_10; }
	inline void set_inputs_10(Queue_1_t1275872360 * value)
	{
		___inputs_10 = value;
		Il2CppCodeGenWriteBarrier((&___inputs_10), value);
	}

	inline static int32_t get_offset_of_blockIsMoving_11() { return static_cast<int32_t>(offsetof(Puzzle_t2606799644, ___blockIsMoving_11)); }
	inline bool get_blockIsMoving_11() const { return ___blockIsMoving_11; }
	inline bool* get_address_of_blockIsMoving_11() { return &___blockIsMoving_11; }
	inline void set_blockIsMoving_11(bool value)
	{
		___blockIsMoving_11 = value;
	}

	inline static int32_t get_offset_of_shuffleMovesRemaining_12() { return static_cast<int32_t>(offsetof(Puzzle_t2606799644, ___shuffleMovesRemaining_12)); }
	inline int32_t get_shuffleMovesRemaining_12() const { return ___shuffleMovesRemaining_12; }
	inline int32_t* get_address_of_shuffleMovesRemaining_12() { return &___shuffleMovesRemaining_12; }
	inline void set_shuffleMovesRemaining_12(int32_t value)
	{
		___shuffleMovesRemaining_12 = value;
	}

	inline static int32_t get_offset_of_prevShuffleOffset_13() { return static_cast<int32_t>(offsetof(Puzzle_t2606799644, ___prevShuffleOffset_13)); }
	inline Vector2Int_t3469998543  get_prevShuffleOffset_13() const { return ___prevShuffleOffset_13; }
	inline Vector2Int_t3469998543 * get_address_of_prevShuffleOffset_13() { return &___prevShuffleOffset_13; }
	inline void set_prevShuffleOffset_13(Vector2Int_t3469998543  value)
	{
		___prevShuffleOffset_13 = value;
	}

	inline static int32_t get_offset_of_shufle_14() { return static_cast<int32_t>(offsetof(Puzzle_t2606799644, ___shufle_14)); }
	inline bool get_shufle_14() const { return ___shufle_14; }
	inline bool* get_address_of_shufle_14() { return &___shufle_14; }
	inline void set_shufle_14(bool value)
	{
		___shufle_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PUZZLE_T2606799644_H
#ifndef BLOCK_T1429612866_H
#define BLOCK_T1429612866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Block
struct  Block_t1429612866  : public MonoBehaviour_t3962482529
{
public:
	// System.Action`1<Block> Block::OnBlockPressed
	Action_1_t1602080461 * ___OnBlockPressed_2;
	// System.Action Block::OnFinishedMoving
	Action_t1264377477 * ___OnFinishedMoving_3;
	// UnityEngine.Vector2Int Block::coord
	Vector2Int_t3469998543  ___coord_4;
	// UnityEngine.Vector2Int Block::startingCoord
	Vector2Int_t3469998543  ___startingCoord_5;

public:
	inline static int32_t get_offset_of_OnBlockPressed_2() { return static_cast<int32_t>(offsetof(Block_t1429612866, ___OnBlockPressed_2)); }
	inline Action_1_t1602080461 * get_OnBlockPressed_2() const { return ___OnBlockPressed_2; }
	inline Action_1_t1602080461 ** get_address_of_OnBlockPressed_2() { return &___OnBlockPressed_2; }
	inline void set_OnBlockPressed_2(Action_1_t1602080461 * value)
	{
		___OnBlockPressed_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnBlockPressed_2), value);
	}

	inline static int32_t get_offset_of_OnFinishedMoving_3() { return static_cast<int32_t>(offsetof(Block_t1429612866, ___OnFinishedMoving_3)); }
	inline Action_t1264377477 * get_OnFinishedMoving_3() const { return ___OnFinishedMoving_3; }
	inline Action_t1264377477 ** get_address_of_OnFinishedMoving_3() { return &___OnFinishedMoving_3; }
	inline void set_OnFinishedMoving_3(Action_t1264377477 * value)
	{
		___OnFinishedMoving_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnFinishedMoving_3), value);
	}

	inline static int32_t get_offset_of_coord_4() { return static_cast<int32_t>(offsetof(Block_t1429612866, ___coord_4)); }
	inline Vector2Int_t3469998543  get_coord_4() const { return ___coord_4; }
	inline Vector2Int_t3469998543 * get_address_of_coord_4() { return &___coord_4; }
	inline void set_coord_4(Vector2Int_t3469998543  value)
	{
		___coord_4 = value;
	}

	inline static int32_t get_offset_of_startingCoord_5() { return static_cast<int32_t>(offsetof(Block_t1429612866, ___startingCoord_5)); }
	inline Vector2Int_t3469998543  get_startingCoord_5() const { return ___startingCoord_5; }
	inline Vector2Int_t3469998543 * get_address_of_startingCoord_5() { return &___startingCoord_5; }
	inline void set_startingCoord_5(Vector2Int_t3469998543  value)
	{
		___startingCoord_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCK_T1429612866_H
// UnityEngine.Texture2D[0...,0...]
struct Texture2DU5B0___U2C0___U5D_t149664597  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Texture2D_t3840446185 * m_Items[1];

public:
	inline Texture2D_t3840446185 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Texture2D_t3840446185 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Texture2D_t3840446185 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Texture2D_t3840446185 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Texture2D_t3840446185 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Texture2D_t3840446185 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Texture2D_t3840446185 * GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline Texture2D_t3840446185 ** GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, Texture2D_t3840446185 * value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Texture2D_t3840446185 * GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline Texture2D_t3840446185 ** GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, Texture2D_t3840446185 * value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t2555686324  m_Items[1];

public:
	inline Color_t2555686324  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t2555686324  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t2555686324 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t2555686324  value)
	{
		m_Items[index] = value;
	}
};
// Block[0...,0...]
struct BlockU5B0___U2C0___U5D_t2917470072  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Block_t1429612866 * m_Items[1];

public:
	inline Block_t1429612866 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Block_t1429612866 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Block_t1429612866 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Block_t1429612866 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Block_t1429612866 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Block_t1429612866 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Block_t1429612866 * GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline Block_t1429612866 ** GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, Block_t1429612866 * value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Block_t1429612866 * GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline Block_t1429612866 ** GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, Block_t1429612866 * value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector2Int[]
struct Vector2IntU5BU5D_t2878452246  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2Int_t3469998543  m_Items[1];

public:
	inline Vector2Int_t3469998543  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2Int_t3469998543 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2Int_t3469998543  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2Int_t3469998543  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2Int_t3469998543 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2Int_t3469998543  value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  RuntimeObject * Component_GetComponent_TisRuntimeObject_m2906321015_gshared (Component_t1923634451 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  RuntimeObject * Resources_Load_TisRuntimeObject_m597869152_gshared (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::Invoke(!0)
extern "C"  void Action_1_Invoke_m2461023210_gshared (Action_1_t3252573759 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  RuntimeObject * GameObject_AddComponent_TisRuntimeObject_m973870487_gshared (GameObject_t1113636619 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m118522912_gshared (Action_1_t3252573759 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<System.Object>::.ctor()
extern "C"  void Queue_1__ctor_m3749217910_gshared (Queue_1_t2926365658 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<System.Object>::Enqueue(!0)
extern "C"  void Queue_1_Enqueue_m1868480850_gshared (Queue_1_t2926365658 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<System.Object>::Dequeue()
extern "C"  RuntimeObject * Queue_1_Dequeue_m3550993416_gshared (Queue_1_t2926365658 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Queue`1<System.Object>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m2496300460_gshared (Queue_1_t2926365658 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Combine_m1859655160 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Remove_m334097152 (RuntimeObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshRenderer>()
#define Component_GetComponent_TisMeshRenderer_t587009260_m2899624428(__this, method) ((  MeshRenderer_t587009260 * (*) (Component_t1923634451 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m2906321015_gshared)(__this, method)
// !!0 UnityEngine.Resources::Load<UnityEngine.Material>(System.String)
#define Resources_Load_TisMaterial_t340375123_m2720476655(__this /* static, unused */, p0, method) ((  Material_t340375123 * (*) (RuntimeObject * /* static, unused */, String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m597869152_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C"  void Renderer_set_material_m1157964140 (Renderer_t2627027031 * __this, Material_t340375123 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t340375123 * Renderer_get_material_m4171603682 (Renderer_t2627027031 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
extern "C"  void Material_set_mainTexture_m544811714 (Material_t340375123 * __this, Texture_t3661962703 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Block::AnimateMove(UnityEngine.Vector2,System.Single)
extern "C"  RuntimeObject* Block_AnimateMove_m3207370942 (Block_t1429612866 * __this, Vector2_t2156229523  ___target0, float ___duration1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C"  Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<Block>::Invoke(!0)
#define Action_1_Invoke_m1342042192(__this, p0, method) ((  void (*) (Action_1_t1602080461 *, Block_t1429612866 *, const RuntimeMethod*))Action_1_Invoke_m2461023210_gshared)(__this, p0, method)
// System.Void Block/<AnimateMove>c__Iterator0::.ctor()
extern "C"  void U3CAnimateMoveU3Ec__Iterator0__ctor_m4034960487 (U3CAnimateMoveU3Ec__Iterator0_t300133182 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector2Int::op_Equality(UnityEngine.Vector2Int,UnityEngine.Vector2Int)
extern "C"  bool Vector2Int_op_Equality_m1080880831 (RuntimeObject * __this /* static, unused */, Vector2Int_t3469998543  p0, Vector2Int_t3469998543  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m3162698980 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m36019626 (Transform_t3600365921 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  Vector2_op_Implicit_m4260192859 (RuntimeObject * __this /* static, unused */, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m372706562 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::Lerp(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2156229523  Vector2_Lerp_m854472224 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, float p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t3722313464  Vector2_op_Implicit_m1860157806 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m3387557959 (Transform_t3600365921 * __this, Vector3_t3722313464  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action::Invoke()
extern "C"  void Action_Invoke_m937035532 (Action_t1264377477 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Min_m18103608 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern "C"  void Texture2D__ctor_m373113269 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern "C"  void Texture_set_wrapMode_m587872754 (Texture_t3661962703 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  ColorU5BU5D_t941916413* Texture2D_GetPixels_m255035209 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
extern "C"  void Texture2D_SetPixels_m3008871897 (Texture2D_t3840446185 * __this, ColorU5BU5D_t941916413* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Apply()
extern "C"  void Texture2D_Apply_m2271746283 (Texture2D_t3840446185 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Puzzle::CreatePuzzle()
extern "C"  void Puzzle_CreatePuzzle_m3554627909 (Puzzle_t2606799644 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Puzzle::StartShuffle()
extern "C"  void Puzzle_StartShuffle_m3525740025 (Puzzle_t2606799644 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D[0...,0...] ImageSlicer::GetSlices(UnityEngine.Texture2D,System.Int32)
extern "C"  Texture2DU5B0___U2C0___U5D_t149664597* ImageSlicer_GetSlices_m2783497285 (RuntimeObject * __this /* static, unused */, Texture2D_t3840446185 * ___image0, int32_t ___blocksPerLine1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::CreatePrimitive(UnityEngine.PrimitiveType)
extern "C"  GameObject_t1113636619 * GameObject_CreatePrimitive_m2902598419 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C"  Transform_t3600365921 * GameObject_get_transform_m1369836730 (GameObject_t1113636619 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C"  Vector2_t2156229523  Vector2_get_one_m738793577 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_UnaryNegation(UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_op_UnaryNegation_m2172448356 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t2156229523  Vector2_op_Multiply_m2347887432 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3970636864 (Vector2_t2156229523 * __this, float p0, float p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_op_Addition_m800700293 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, Vector2_t2156229523  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern "C"  void Transform_set_parent_m786917804 (Transform_t3600365921 * __this, Transform_t3600365921 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::AddComponent<Block>()
#define GameObject_AddComponent_TisBlock_t1429612866_m2794020862(__this, method) ((  Block_t1429612866 * (*) (GameObject_t1113636619 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_m973870487_gshared)(__this, method)
// System.Void System.Action`1<Block>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m1790430673(__this, p0, p1, method) ((  void (*) (Action_1_t1602080461 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m118522912_gshared)(__this, p0, p1, method)
// System.Void Block::add_OnBlockPressed(System.Action`1<Block>)
extern "C"  void Block_add_OnBlockPressed_m238060131 (Block_t1429612866 * __this, Action_1_t1602080461 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
extern "C"  void Action__ctor_m2994342681 (Action_t1264377477 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Block::add_OnFinishedMoving(System.Action)
extern "C"  void Block_add_OnFinishedMoving_m2464650543 (Block_t1429612866 * __this, Action_t1264377477 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2Int::.ctor(System.Int32,System.Int32)
extern "C"  void Vector2Int__ctor_m3872920888 (Vector2Int_t3469998543 * __this, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Block::Init(UnityEngine.Vector2Int,UnityEngine.Texture2D)
extern "C"  void Block_Init_m3829536269 (Block_t1429612866 * __this, Vector2Int_t3469998543  ___startingCoord0, Texture2D_t3840446185 * ___image1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t4157153871 * Camera_get_main_m3643453163 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C"  void Camera_set_orthographicSize_m76971700 (Camera_t4157153871 * __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Queue`1<Block>::.ctor()
#define Queue_1__ctor_m2416854755(__this, method) ((  void (*) (Queue_1_t1275872360 *, const RuntimeMethod*))Queue_1__ctor_m3749217910_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1<Block>::Enqueue(!0)
#define Queue_1_Enqueue_m2019291069(__this, p0, method) ((  void (*) (Queue_1_t1275872360 *, Block_t1429612866 *, const RuntimeMethod*))Queue_1_Enqueue_m1868480850_gshared)(__this, p0, method)
// System.Void Puzzle::MakeNextPlayerMove()
extern "C"  void Puzzle_MakeNextPlayerMove_m3707527570 (Puzzle_t2606799644 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.Queue`1<Block>::Dequeue()
#define Queue_1_Dequeue_m1430048205(__this, method) ((  Block_t1429612866 * (*) (Queue_1_t1275872360 *, const RuntimeMethod*))Queue_1_Dequeue_m3550993416_gshared)(__this, method)
// System.Void Puzzle::MoveBlock(Block,System.Single)
extern "C"  void Puzzle_MoveBlock_m1974819708 (Puzzle_t2606799644 * __this, Block_t1429612866 * ___blockToMove0, float ___duration1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Queue`1<Block>::get_Count()
#define Queue_1_get_Count_m896737555(__this, method) ((  int32_t (*) (Queue_1_t1275872360 *, const RuntimeMethod*))Queue_1_get_Count_m2496300460_gshared)(__this, method)
// UnityEngine.Vector2Int UnityEngine.Vector2Int::op_Subtraction(UnityEngine.Vector2Int,UnityEngine.Vector2Int)
extern "C"  Vector2Int_t3469998543  Vector2Int_op_Subtraction_m2233598263 (RuntimeObject * __this /* static, unused */, Vector2Int_t3469998543  p0, Vector2Int_t3469998543  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector2Int::get_sqrMagnitude()
extern "C"  int32_t Vector2Int_get_sqrMagnitude_m622024585 (Vector2Int_t3469998543 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector2Int::get_x()
extern "C"  int32_t Vector2Int_get_x_m64542184 (Vector2Int_t3469998543 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector2Int::get_y()
extern "C"  int32_t Vector2Int_get_y_m64542185 (Vector2Int_t3469998543 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Block::MoveToPosition(UnityEngine.Vector2,System.Single)
extern "C"  void Block_MoveToPosition_m2829428912 (Block_t1429612866 * __this, Vector2_t2156229523  ___target0, float ___duration1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Puzzle::CheckIfSolved()
extern "C"  void Puzzle_CheckIfSolved_m1788021871 (Puzzle_t2606799644 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Puzzle::MakeNextShuffleMove()
extern "C"  void Puzzle_MakeNextShuffleMove_m3103018245 (Puzzle_t2606799644 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m442555142 (Component_t1923634451 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m796801857 (GameObject_t1113636619 * __this, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m4054026115 (RuntimeObject * __this /* static, unused */, int32_t p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2Int UnityEngine.Vector2Int::op_Multiply(UnityEngine.Vector2Int,System.Int32)
extern "C"  Vector2Int_t3469998543  Vector2Int_op_Multiply_m2509535640 (RuntimeObject * __this /* static, unused */, Vector2Int_t3469998543  p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector2Int::op_Inequality(UnityEngine.Vector2Int,UnityEngine.Vector2Int)
extern "C"  bool Vector2Int_op_Inequality_m156856833 (RuntimeObject * __this /* static, unused */, Vector2Int_t3469998543  p0, Vector2Int_t3469998543  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2Int UnityEngine.Vector2Int::op_Addition(UnityEngine.Vector2Int,UnityEngine.Vector2Int)
extern "C"  Vector2Int_t3469998543  Vector2Int_op_Addition_m1244329832 (RuntimeObject * __this /* static, unused */, Vector2Int_t3469998543  p0, Vector2Int_t3469998543  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::GetLength(System.Int32)
extern "C"  int32_t Array_GetLength_m2178203778 (RuntimeArray * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean Block::IsAtStartingCoord()
extern "C"  bool Block_IsAtStartingCoord_m1016848544 (Block_t1429612866 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Block::.ctor()
extern "C"  void Block__ctor_m2599590690 (Block_t1429612866 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Block::add_OnBlockPressed(System.Action`1<Block>)
extern "C"  void Block_add_OnBlockPressed_m238060131 (Block_t1429612866 * __this, Action_1_t1602080461 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Block_add_OnBlockPressed_m238060131_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t1602080461 * V_0 = NULL;
	Action_1_t1602080461 * V_1 = NULL;
	{
		Action_1_t1602080461 * L_0 = __this->get_OnBlockPressed_2();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t1602080461 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t1602080461 ** L_2 = __this->get_address_of_OnBlockPressed_2();
		Action_1_t1602080461 * L_3 = V_1;
		Action_1_t1602080461 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Action_1_t1602080461 * L_6 = V_0;
		Action_1_t1602080461 * L_7 = InterlockedCompareExchangeImpl<Action_1_t1602080461 *>(L_2, ((Action_1_t1602080461 *)CastclassSealed((RuntimeObject*)L_5, Action_1_t1602080461_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		Action_1_t1602080461 * L_8 = V_0;
		Action_1_t1602080461 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t1602080461 *)L_8) == ((RuntimeObject*)(Action_1_t1602080461 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Block::remove_OnBlockPressed(System.Action`1<Block>)
extern "C"  void Block_remove_OnBlockPressed_m3588265933 (Block_t1429612866 * __this, Action_1_t1602080461 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Block_remove_OnBlockPressed_m3588265933_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t1602080461 * V_0 = NULL;
	Action_1_t1602080461 * V_1 = NULL;
	{
		Action_1_t1602080461 * L_0 = __this->get_OnBlockPressed_2();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_1_t1602080461 * L_1 = V_0;
		V_1 = L_1;
		Action_1_t1602080461 ** L_2 = __this->get_address_of_OnBlockPressed_2();
		Action_1_t1602080461 * L_3 = V_1;
		Action_1_t1602080461 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Action_1_t1602080461 * L_6 = V_0;
		Action_1_t1602080461 * L_7 = InterlockedCompareExchangeImpl<Action_1_t1602080461 *>(L_2, ((Action_1_t1602080461 *)CastclassSealed((RuntimeObject*)L_5, Action_1_t1602080461_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		Action_1_t1602080461 * L_8 = V_0;
		Action_1_t1602080461 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_1_t1602080461 *)L_8) == ((RuntimeObject*)(Action_1_t1602080461 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Block::add_OnFinishedMoving(System.Action)
extern "C"  void Block_add_OnFinishedMoving_m2464650543 (Block_t1429612866 * __this, Action_t1264377477 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Block_add_OnFinishedMoving_m2464650543_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_t1264377477 * V_0 = NULL;
	Action_t1264377477 * V_1 = NULL;
	{
		Action_t1264377477 * L_0 = __this->get_OnFinishedMoving_3();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_t1264377477 * L_1 = V_0;
		V_1 = L_1;
		Action_t1264377477 ** L_2 = __this->get_address_of_OnFinishedMoving_3();
		Action_t1264377477 * L_3 = V_1;
		Action_t1264377477 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Action_t1264377477 * L_6 = V_0;
		Action_t1264377477 * L_7 = InterlockedCompareExchangeImpl<Action_t1264377477 *>(L_2, ((Action_t1264377477 *)CastclassSealed((RuntimeObject*)L_5, Action_t1264377477_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		Action_t1264377477 * L_8 = V_0;
		Action_t1264377477 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_t1264377477 *)L_8) == ((RuntimeObject*)(Action_t1264377477 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Block::remove_OnFinishedMoving(System.Action)
extern "C"  void Block_remove_OnFinishedMoving_m1922473105 (Block_t1429612866 * __this, Action_t1264377477 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Block_remove_OnFinishedMoving_m1922473105_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_t1264377477 * V_0 = NULL;
	Action_t1264377477 * V_1 = NULL;
	{
		Action_t1264377477 * L_0 = __this->get_OnFinishedMoving_3();
		V_0 = L_0;
	}

IL_0007:
	{
		Action_t1264377477 * L_1 = V_0;
		V_1 = L_1;
		Action_t1264377477 ** L_2 = __this->get_address_of_OnFinishedMoving_3();
		Action_t1264377477 * L_3 = V_1;
		Action_t1264377477 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Action_t1264377477 * L_6 = V_0;
		Action_t1264377477 * L_7 = InterlockedCompareExchangeImpl<Action_t1264377477 *>(L_2, ((Action_t1264377477 *)CastclassSealed((RuntimeObject*)L_5, Action_t1264377477_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		Action_t1264377477 * L_8 = V_0;
		Action_t1264377477 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Action_t1264377477 *)L_8) == ((RuntimeObject*)(Action_t1264377477 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Block::Init(UnityEngine.Vector2Int,UnityEngine.Texture2D)
extern "C"  void Block_Init_m3829536269 (Block_t1429612866 * __this, Vector2Int_t3469998543  ___startingCoord0, Texture2D_t3840446185 * ___image1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Block_Init_m3829536269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector2Int_t3469998543  L_0 = ___startingCoord0;
		__this->set_startingCoord_5(L_0);
		Vector2Int_t3469998543  L_1 = ___startingCoord0;
		__this->set_coord_4(L_1);
		MeshRenderer_t587009260 * L_2 = Component_GetComponent_TisMeshRenderer_t587009260_m2899624428(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t587009260_m2899624428_RuntimeMethod_var);
		Material_t340375123 * L_3 = Resources_Load_TisMaterial_t340375123_m2720476655(NULL /*static, unused*/, _stringLiteral424788472, /*hidden argument*/Resources_Load_TisMaterial_t340375123_m2720476655_RuntimeMethod_var);
		NullCheck(L_2);
		Renderer_set_material_m1157964140(L_2, L_3, /*hidden argument*/NULL);
		MeshRenderer_t587009260 * L_4 = Component_GetComponent_TisMeshRenderer_t587009260_m2899624428(__this, /*hidden argument*/Component_GetComponent_TisMeshRenderer_t587009260_m2899624428_RuntimeMethod_var);
		NullCheck(L_4);
		Material_t340375123 * L_5 = Renderer_get_material_m4171603682(L_4, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_6 = ___image1;
		NullCheck(L_5);
		Material_set_mainTexture_m544811714(L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Block::MoveToPosition(UnityEngine.Vector2,System.Single)
extern "C"  void Block_MoveToPosition_m2829428912 (Block_t1429612866 * __this, Vector2_t2156229523  ___target0, float ___duration1, const RuntimeMethod* method)
{
	{
		Vector2_t2156229523  L_0 = ___target0;
		float L_1 = ___duration1;
		RuntimeObject* L_2 = Block_AnimateMove_m3207370942(__this, L_0, L_1, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Block::OnMouseDown()
extern "C"  void Block_OnMouseDown_m2987425322 (Block_t1429612866 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Block_OnMouseDown_m2987425322_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Action_1_t1602080461 * L_0 = __this->get_OnBlockPressed_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t1602080461 * L_1 = __this->get_OnBlockPressed_2();
		NullCheck(L_1);
		Action_1_Invoke_m1342042192(L_1, __this, /*hidden argument*/Action_1_Invoke_m1342042192_RuntimeMethod_var);
	}

IL_0017:
	{
		return;
	}
}
// System.Collections.IEnumerator Block::AnimateMove(UnityEngine.Vector2,System.Single)
extern "C"  RuntimeObject* Block_AnimateMove_m3207370942 (Block_t1429612866 * __this, Vector2_t2156229523  ___target0, float ___duration1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Block_AnimateMove_m3207370942_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CAnimateMoveU3Ec__Iterator0_t300133182 * V_0 = NULL;
	{
		U3CAnimateMoveU3Ec__Iterator0_t300133182 * L_0 = (U3CAnimateMoveU3Ec__Iterator0_t300133182 *)il2cpp_codegen_object_new(U3CAnimateMoveU3Ec__Iterator0_t300133182_il2cpp_TypeInfo_var);
		U3CAnimateMoveU3Ec__Iterator0__ctor_m4034960487(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAnimateMoveU3Ec__Iterator0_t300133182 * L_1 = V_0;
		float L_2 = ___duration1;
		NullCheck(L_1);
		L_1->set_duration_2(L_2);
		U3CAnimateMoveU3Ec__Iterator0_t300133182 * L_3 = V_0;
		Vector2_t2156229523  L_4 = ___target0;
		NullCheck(L_3);
		L_3->set_target_3(L_4);
		U3CAnimateMoveU3Ec__Iterator0_t300133182 * L_5 = V_0;
		NullCheck(L_5);
		L_5->set_U24this_4(__this);
		U3CAnimateMoveU3Ec__Iterator0_t300133182 * L_6 = V_0;
		return L_6;
	}
}
// System.Boolean Block::IsAtStartingCoord()
extern "C"  bool Block_IsAtStartingCoord_m1016848544 (Block_t1429612866 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Block_IsAtStartingCoord_m1016848544_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector2Int_t3469998543  L_0 = __this->get_coord_4();
		Vector2Int_t3469998543  L_1 = __this->get_startingCoord_5();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2Int_t3469998543_il2cpp_TypeInfo_var);
		bool L_2 = Vector2Int_op_Equality_m1080880831(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Block/<AnimateMove>c__Iterator0::.ctor()
extern "C"  void U3CAnimateMoveU3Ec__Iterator0__ctor_m4034960487 (U3CAnimateMoveU3Ec__Iterator0_t300133182 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Block/<AnimateMove>c__Iterator0::MoveNext()
extern "C"  bool U3CAnimateMoveU3Ec__Iterator0_MoveNext_m2844285960 (U3CAnimateMoveU3Ec__Iterator0_t300133182 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAnimateMoveU3Ec__Iterator0_MoveNext_m2844285960_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_7();
		V_0 = L_0;
		__this->set_U24PC_7((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_00ac;
			}
		}
	}
	{
		goto IL_00e3;
	}

IL_0021:
	{
		Block_t1429612866 * L_2 = __this->get_U24this_4();
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m3162698980(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3722313464  L_4 = Transform_get_position_m36019626(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_5 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_U3CinitialPosU3E__0_0(L_5);
		__this->set_U3CpercentU3E__0_1((0.0f));
		goto IL_00ac;
	}

IL_004c:
	{
		float L_6 = __this->get_U3CpercentU3E__0_1();
		float L_7 = Time_get_deltaTime_m372706562(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = __this->get_duration_2();
		__this->set_U3CpercentU3E__0_1(((float)il2cpp_codegen_add((float)L_6, (float)((float)((float)L_7/(float)L_8)))));
		Block_t1429612866 * L_9 = __this->get_U24this_4();
		NullCheck(L_9);
		Transform_t3600365921 * L_10 = Component_get_transform_m3162698980(L_9, /*hidden argument*/NULL);
		Vector2_t2156229523  L_11 = __this->get_U3CinitialPosU3E__0_0();
		Vector2_t2156229523  L_12 = __this->get_target_3();
		float L_13 = __this->get_U3CpercentU3E__0_1();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_14 = Vector2_Lerp_m854472224(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		Vector3_t3722313464  L_15 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_set_position_m3387557959(L_10, L_15, /*hidden argument*/NULL);
		__this->set_U24current_5(NULL);
		bool L_16 = __this->get_U24disposing_6();
		if (L_16)
		{
			goto IL_00a7;
		}
	}
	{
		__this->set_U24PC_7(1);
	}

IL_00a7:
	{
		goto IL_00e5;
	}

IL_00ac:
	{
		float L_17 = __this->get_U3CpercentU3E__0_1();
		if ((((float)L_17) < ((float)(1.0f))))
		{
			goto IL_004c;
		}
	}
	{
		Block_t1429612866 * L_18 = __this->get_U24this_4();
		NullCheck(L_18);
		Action_t1264377477 * L_19 = L_18->get_OnFinishedMoving_3();
		if (!L_19)
		{
			goto IL_00dc;
		}
	}
	{
		Block_t1429612866 * L_20 = __this->get_U24this_4();
		NullCheck(L_20);
		Action_t1264377477 * L_21 = L_20->get_OnFinishedMoving_3();
		NullCheck(L_21);
		Action_Invoke_m937035532(L_21, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		__this->set_U24PC_7((-1));
	}

IL_00e3:
	{
		return (bool)0;
	}

IL_00e5:
	{
		return (bool)1;
	}
}
// System.Object Block/<AnimateMove>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  RuntimeObject * U3CAnimateMoveU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3522699963 (U3CAnimateMoveU3Ec__Iterator0_t300133182 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object Block/<AnimateMove>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * U3CAnimateMoveU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1167992478 (U3CAnimateMoveU3Ec__Iterator0_t300133182 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Void Block/<AnimateMove>c__Iterator0::Dispose()
extern "C"  void U3CAnimateMoveU3Ec__Iterator0_Dispose_m2108281967 (U3CAnimateMoveU3Ec__Iterator0_t300133182 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_6((bool)1);
		__this->set_U24PC_7((-1));
		return;
	}
}
// System.Void Block/<AnimateMove>c__Iterator0::Reset()
extern "C"  void U3CAnimateMoveU3Ec__Iterator0_Reset_m1788849239 (U3CAnimateMoveU3Ec__Iterator0_t300133182 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CAnimateMoveU3Ec__Iterator0_Reset_m1788849239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0,U3CAnimateMoveU3Ec__Iterator0_Reset_m1788849239_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Texture2D[0...,0...] ImageSlicer::GetSlices(UnityEngine.Texture2D,System.Int32)
extern "C"  Texture2DU5B0___U2C0___U5D_t149664597* ImageSlicer_GetSlices_m2783497285 (RuntimeObject * __this /* static, unused */, Texture2D_t3840446185 * ___image0, int32_t ___blocksPerLine1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ImageSlicer_GetSlices_m2783497285_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Texture2DU5B0___U2C0___U5D_t149664597* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Texture2D_t3840446185 * V_5 = NULL;
	{
		Texture2D_t3840446185 * L_0 = ___image0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_0);
		Texture2D_t3840446185 * L_2 = ___image0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_Min_m18103608(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		int32_t L_6 = ___blocksPerLine1;
		V_1 = ((int32_t)((int32_t)L_5/(int32_t)L_6));
		int32_t L_7 = ___blocksPerLine1;
		int32_t L_8 = ___blocksPerLine1;
		il2cpp_array_size_t L_10[] = { (il2cpp_array_size_t)L_7, (il2cpp_array_size_t)L_8 };
		Texture2DU5B0___U2C0___U5D_t149664597* L_9 = (Texture2DU5B0___U2C0___U5D_t149664597*)GenArrayNew(Texture2DU5B0___U2C0___U5D_t149664597_il2cpp_TypeInfo_var, L_10);
		V_2 = L_9;
		V_3 = 0;
		goto IL_0078;
	}

IL_0025:
	{
		V_4 = 0;
		goto IL_006c;
	}

IL_002d:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_1;
		Texture2D_t3840446185 * L_13 = (Texture2D_t3840446185 *)il2cpp_codegen_object_new(Texture2D_t3840446185_il2cpp_TypeInfo_var);
		Texture2D__ctor_m373113269(L_13, L_11, L_12, /*hidden argument*/NULL);
		V_5 = L_13;
		Texture2D_t3840446185 * L_14 = V_5;
		NullCheck(L_14);
		Texture_set_wrapMode_m587872754(L_14, 1, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_15 = V_5;
		Texture2D_t3840446185 * L_16 = ___image0;
		int32_t L_17 = V_4;
		int32_t L_18 = V_1;
		int32_t L_19 = V_3;
		int32_t L_20 = V_1;
		int32_t L_21 = V_1;
		int32_t L_22 = V_1;
		NullCheck(L_16);
		ColorU5BU5D_t941916413* L_23 = Texture2D_GetPixels_m255035209(L_16, ((int32_t)il2cpp_codegen_multiply((int32_t)L_17, (int32_t)L_18)), ((int32_t)il2cpp_codegen_multiply((int32_t)L_19, (int32_t)L_20)), L_21, L_22, /*hidden argument*/NULL);
		NullCheck(L_15);
		Texture2D_SetPixels_m3008871897(L_15, L_23, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_24 = V_5;
		NullCheck(L_24);
		Texture2D_Apply_m2271746283(L_24, /*hidden argument*/NULL);
		Texture2DU5B0___U2C0___U5D_t149664597* L_25 = V_2;
		int32_t L_26 = V_4;
		int32_t L_27 = V_3;
		Texture2D_t3840446185 * L_28 = V_5;
		NullCheck(L_25);
		(L_25)->SetAt(L_26, L_27, L_28);
		int32_t L_29 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_29, (int32_t)1));
	}

IL_006c:
	{
		int32_t L_30 = V_4;
		int32_t L_31 = ___blocksPerLine1;
		if ((((int32_t)L_30) < ((int32_t)L_31)))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_32 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1));
	}

IL_0078:
	{
		int32_t L_33 = V_3;
		int32_t L_34 = ___blocksPerLine1;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_0025;
		}
	}
	{
		Texture2DU5B0___U2C0___U5D_t149664597* L_35 = V_2;
		return L_35;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Puzzle::.ctor()
extern "C"  void Puzzle__ctor_m3135826562 (Puzzle_t2606799644 * __this, const RuntimeMethod* method)
{
	{
		__this->set_blocksPerLine_3(4);
		__this->set_shuffleLength_4(((int32_t)20));
		__this->set_defaultMoveDuration_5((0.2f));
		__this->set_shuffleMoveDuration_6((0.1f));
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Puzzle::Start()
extern "C"  void Puzzle_Start_m2919735906 (Puzzle_t2606799644 * __this, const RuntimeMethod* method)
{
	{
		Puzzle_CreatePuzzle_m3554627909(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Puzzle::Update()
extern "C"  void Puzzle_Update_m2472942782 (Puzzle_t2606799644 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_state_7();
		if (L_0)
		{
			goto IL_0023;
		}
	}
	{
		bool L_1 = __this->get_shufle_14();
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		Puzzle_StartShuffle_m3525740025(__this, /*hidden argument*/NULL);
		__this->set_shufle_14((bool)1);
	}

IL_0023:
	{
		return;
	}
}
// System.Void Puzzle::CreatePuzzle()
extern "C"  void Puzzle_CreatePuzzle_m3554627909 (Puzzle_t2606799644 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Puzzle_CreatePuzzle_m3554627909_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Texture2DU5B0___U2C0___U5D_t149664597* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	GameObject_t1113636619 * V_3 = NULL;
	Block_t1429612866 * V_4 = NULL;
	{
		int32_t L_0 = __this->get_blocksPerLine_3();
		int32_t L_1 = __this->get_blocksPerLine_3();
		il2cpp_array_size_t L_3[] = { (il2cpp_array_size_t)L_0, (il2cpp_array_size_t)L_1 };
		BlockU5B0___U2C0___U5D_t2917470072* L_2 = (BlockU5B0___U2C0___U5D_t2917470072*)GenArrayNew(BlockU5B0___U2C0___U5D_t2917470072_il2cpp_TypeInfo_var, L_3);
		__this->set_blocks_9((BlockU5B0___U2C0___U5D_t2917470072*)L_2);
		Texture2D_t3840446185 * L_4 = __this->get_image_2();
		int32_t L_5 = __this->get_blocksPerLine_3();
		Texture2DU5B0___U2C0___U5D_t149664597* L_6 = ImageSlicer_GetSlices_m2783497285(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		V_1 = 0;
		goto IL_0112;
	}

IL_0030:
	{
		V_2 = 0;
		goto IL_0102;
	}

IL_0037:
	{
		GameObject_t1113636619 * L_7 = GameObject_CreatePrimitive_m2902598419(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
		V_3 = L_7;
		GameObject_t1113636619 * L_8 = V_3;
		NullCheck(L_8);
		Transform_t3600365921 * L_9 = GameObject_get_transform_m1369836730(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_10 = Vector2_get_one_m738793577(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2156229523  L_11 = Vector2_op_UnaryNegation_m2172448356(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_12 = __this->get_blocksPerLine_3();
		Vector2_t2156229523  L_13 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_11, (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_12, (int32_t)1))))), /*hidden argument*/NULL);
		Vector2_t2156229523  L_14 = Vector2_op_Multiply_m2347887432(NULL /*static, unused*/, L_13, (0.5f), /*hidden argument*/NULL);
		int32_t L_15 = V_2;
		int32_t L_16 = V_1;
		Vector2_t2156229523  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector2__ctor_m3970636864((&L_17), (((float)((float)L_15))), (((float)((float)L_16))), /*hidden argument*/NULL);
		Vector2_t2156229523  L_18 = Vector2_op_Addition_m800700293(NULL /*static, unused*/, L_14, L_17, /*hidden argument*/NULL);
		Vector3_t3722313464  L_19 = Vector2_op_Implicit_m1860157806(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_position_m3387557959(L_9, L_19, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_20 = V_3;
		NullCheck(L_20);
		Transform_t3600365921 * L_21 = GameObject_get_transform_m1369836730(L_20, /*hidden argument*/NULL);
		Transform_t3600365921 * L_22 = Component_get_transform_m3162698980(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_parent_m786917804(L_21, L_22, /*hidden argument*/NULL);
		GameObject_t1113636619 * L_23 = V_3;
		NullCheck(L_23);
		Block_t1429612866 * L_24 = GameObject_AddComponent_TisBlock_t1429612866_m2794020862(L_23, /*hidden argument*/GameObject_AddComponent_TisBlock_t1429612866_m2794020862_RuntimeMethod_var);
		V_4 = L_24;
		Block_t1429612866 * L_25 = V_4;
		intptr_t L_26 = (intptr_t)Puzzle_PlayerMoveBlockInput_m630074849_RuntimeMethod_var;
		Action_1_t1602080461 * L_27 = (Action_1_t1602080461 *)il2cpp_codegen_object_new(Action_1_t1602080461_il2cpp_TypeInfo_var);
		Action_1__ctor_m1790430673(L_27, __this, L_26, /*hidden argument*/Action_1__ctor_m1790430673_RuntimeMethod_var);
		NullCheck(L_25);
		Block_add_OnBlockPressed_m238060131(L_25, L_27, /*hidden argument*/NULL);
		Block_t1429612866 * L_28 = V_4;
		intptr_t L_29 = (intptr_t)Puzzle_OnBlockFinishedMoving_m4170377730_RuntimeMethod_var;
		Action_t1264377477 * L_30 = (Action_t1264377477 *)il2cpp_codegen_object_new(Action_t1264377477_il2cpp_TypeInfo_var);
		Action__ctor_m2994342681(L_30, __this, L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		Block_add_OnFinishedMoving_m2464650543(L_28, L_30, /*hidden argument*/NULL);
		Block_t1429612866 * L_31 = V_4;
		int32_t L_32 = V_2;
		int32_t L_33 = V_1;
		Vector2Int_t3469998543  L_34;
		memset(&L_34, 0, sizeof(L_34));
		Vector2Int__ctor_m3872920888((&L_34), L_32, L_33, /*hidden argument*/NULL);
		Texture2DU5B0___U2C0___U5D_t149664597* L_35 = V_0;
		int32_t L_36 = V_2;
		int32_t L_37 = V_1;
		NullCheck(L_35);
		Texture2D_t3840446185 * L_38 = (L_35)->GetAt(L_36, L_37);
		NullCheck(L_31);
		Block_Init_m3829536269(L_31, L_34, L_38, /*hidden argument*/NULL);
		BlockU5B0___U2C0___U5D_t2917470072* L_39 = __this->get_blocks_9();
		int32_t L_40 = V_2;
		int32_t L_41 = V_1;
		Block_t1429612866 * L_42 = V_4;
		NullCheck((BlockU5B0___U2C0___U5D_t2917470072*)(BlockU5B0___U2C0___U5D_t2917470072*)L_39);
		((BlockU5B0___U2C0___U5D_t2917470072*)(BlockU5B0___U2C0___U5D_t2917470072*)L_39)->SetAt(L_40, L_41, L_42);
		int32_t L_43 = V_1;
		if (L_43)
		{
			goto IL_00fe;
		}
	}
	{
		int32_t L_44 = V_2;
		int32_t L_45 = __this->get_blocksPerLine_3();
		if ((!(((uint32_t)L_44) == ((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_45, (int32_t)1))))))
		{
			goto IL_00fe;
		}
	}
	{
		Block_t1429612866 * L_46 = V_4;
		__this->set_emptyBlock_8(L_46);
	}

IL_00fe:
	{
		int32_t L_47 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_47, (int32_t)1));
	}

IL_0102:
	{
		int32_t L_48 = V_2;
		int32_t L_49 = __this->get_blocksPerLine_3();
		if ((((int32_t)L_48) < ((int32_t)L_49)))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_50 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_50, (int32_t)1));
	}

IL_0112:
	{
		int32_t L_51 = V_1;
		int32_t L_52 = __this->get_blocksPerLine_3();
		if ((((int32_t)L_51) < ((int32_t)L_52)))
		{
			goto IL_0030;
		}
	}
	{
		Camera_t4157153871 * L_53 = Camera_get_main_m3643453163(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_54 = __this->get_blocksPerLine_3();
		NullCheck(L_53);
		Camera_set_orthographicSize_m76971700(L_53, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_54))), (float)(0.55f))), /*hidden argument*/NULL);
		Queue_1_t1275872360 * L_55 = (Queue_1_t1275872360 *)il2cpp_codegen_object_new(Queue_1_t1275872360_il2cpp_TypeInfo_var);
		Queue_1__ctor_m2416854755(L_55, /*hidden argument*/Queue_1__ctor_m2416854755_RuntimeMethod_var);
		__this->set_inputs_10(L_55);
		return;
	}
}
// System.Void Puzzle::PlayerMoveBlockInput(Block)
extern "C"  void Puzzle_PlayerMoveBlockInput_m630074849 (Puzzle_t2606799644 * __this, Block_t1429612866 * ___blockToMove0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Puzzle_PlayerMoveBlockInput_m630074849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_state_7();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_001e;
		}
	}
	{
		Queue_1_t1275872360 * L_1 = __this->get_inputs_10();
		Block_t1429612866 * L_2 = ___blockToMove0;
		NullCheck(L_1);
		Queue_1_Enqueue_m2019291069(L_1, L_2, /*hidden argument*/Queue_1_Enqueue_m2019291069_RuntimeMethod_var);
		Puzzle_MakeNextPlayerMove_m3707527570(__this, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Void Puzzle::MakeNextPlayerMove()
extern "C"  void Puzzle_MakeNextPlayerMove_m3707527570 (Puzzle_t2606799644 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Puzzle_MakeNextPlayerMove_m3707527570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		goto IL_001c;
	}

IL_0005:
	{
		Queue_1_t1275872360 * L_0 = __this->get_inputs_10();
		NullCheck(L_0);
		Block_t1429612866 * L_1 = Queue_1_Dequeue_m1430048205(L_0, /*hidden argument*/Queue_1_Dequeue_m1430048205_RuntimeMethod_var);
		float L_2 = __this->get_defaultMoveDuration_5();
		Puzzle_MoveBlock_m1974819708(__this, L_1, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		Queue_1_t1275872360 * L_3 = __this->get_inputs_10();
		NullCheck(L_3);
		int32_t L_4 = Queue_1_get_Count_m896737555(L_3, /*hidden argument*/Queue_1_get_Count_m896737555_RuntimeMethod_var);
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		bool L_5 = __this->get_blockIsMoving_11();
		if (!L_5)
		{
			goto IL_0005;
		}
	}

IL_0038:
	{
		return;
	}
}
// System.Void Puzzle::MoveBlock(Block,System.Single)
extern "C"  void Puzzle_MoveBlock_m1974819708 (Puzzle_t2606799644 * __this, Block_t1429612866 * ___blockToMove0, float ___duration1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Puzzle_MoveBlock_m1974819708_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2Int_t3469998543  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2Int_t3469998543  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Block_t1429612866 * L_0 = ___blockToMove0;
		NullCheck(L_0);
		Vector2Int_t3469998543  L_1 = L_0->get_coord_4();
		Block_t1429612866 * L_2 = __this->get_emptyBlock_8();
		NullCheck(L_2);
		Vector2Int_t3469998543  L_3 = L_2->get_coord_4();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2Int_t3469998543_il2cpp_TypeInfo_var);
		Vector2Int_t3469998543  L_4 = Vector2Int_op_Subtraction_m2233598263(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = Vector2Int_get_sqrMagnitude_m622024585((&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_5) == ((uint32_t)1))))
		{
			goto IL_00db;
		}
	}
	{
		BlockU5B0___U2C0___U5D_t2917470072* L_6 = __this->get_blocks_9();
		Block_t1429612866 * L_7 = ___blockToMove0;
		NullCheck(L_7);
		Vector2Int_t3469998543 * L_8 = L_7->get_address_of_coord_4();
		int32_t L_9 = Vector2Int_get_x_m64542184(L_8, /*hidden argument*/NULL);
		Block_t1429612866 * L_10 = ___blockToMove0;
		NullCheck(L_10);
		Vector2Int_t3469998543 * L_11 = L_10->get_address_of_coord_4();
		int32_t L_12 = Vector2Int_get_y_m64542185(L_11, /*hidden argument*/NULL);
		Block_t1429612866 * L_13 = __this->get_emptyBlock_8();
		NullCheck((BlockU5B0___U2C0___U5D_t2917470072*)(BlockU5B0___U2C0___U5D_t2917470072*)L_6);
		((BlockU5B0___U2C0___U5D_t2917470072*)(BlockU5B0___U2C0___U5D_t2917470072*)L_6)->SetAt(L_9, L_12, L_13);
		BlockU5B0___U2C0___U5D_t2917470072* L_14 = __this->get_blocks_9();
		Block_t1429612866 * L_15 = __this->get_emptyBlock_8();
		NullCheck(L_15);
		Vector2Int_t3469998543 * L_16 = L_15->get_address_of_coord_4();
		int32_t L_17 = Vector2Int_get_x_m64542184(L_16, /*hidden argument*/NULL);
		Block_t1429612866 * L_18 = __this->get_emptyBlock_8();
		NullCheck(L_18);
		Vector2Int_t3469998543 * L_19 = L_18->get_address_of_coord_4();
		int32_t L_20 = Vector2Int_get_y_m64542185(L_19, /*hidden argument*/NULL);
		Block_t1429612866 * L_21 = ___blockToMove0;
		NullCheck((BlockU5B0___U2C0___U5D_t2917470072*)(BlockU5B0___U2C0___U5D_t2917470072*)L_14);
		((BlockU5B0___U2C0___U5D_t2917470072*)(BlockU5B0___U2C0___U5D_t2917470072*)L_14)->SetAt(L_17, L_20, L_21);
		Block_t1429612866 * L_22 = __this->get_emptyBlock_8();
		NullCheck(L_22);
		Vector2Int_t3469998543  L_23 = L_22->get_coord_4();
		V_1 = L_23;
		Block_t1429612866 * L_24 = __this->get_emptyBlock_8();
		Block_t1429612866 * L_25 = ___blockToMove0;
		NullCheck(L_25);
		Vector2Int_t3469998543  L_26 = L_25->get_coord_4();
		NullCheck(L_24);
		L_24->set_coord_4(L_26);
		Block_t1429612866 * L_27 = ___blockToMove0;
		Vector2Int_t3469998543  L_28 = V_1;
		NullCheck(L_27);
		L_27->set_coord_4(L_28);
		Block_t1429612866 * L_29 = __this->get_emptyBlock_8();
		NullCheck(L_29);
		Transform_t3600365921 * L_30 = Component_get_transform_m3162698980(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		Vector3_t3722313464  L_31 = Transform_get_position_m36019626(L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_32 = Vector2_op_Implicit_m4260192859(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		V_2 = L_32;
		Block_t1429612866 * L_33 = __this->get_emptyBlock_8();
		NullCheck(L_33);
		Transform_t3600365921 * L_34 = Component_get_transform_m3162698980(L_33, /*hidden argument*/NULL);
		Block_t1429612866 * L_35 = ___blockToMove0;
		NullCheck(L_35);
		Transform_t3600365921 * L_36 = Component_get_transform_m3162698980(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t3722313464  L_37 = Transform_get_position_m36019626(L_36, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_set_position_m3387557959(L_34, L_37, /*hidden argument*/NULL);
		Block_t1429612866 * L_38 = ___blockToMove0;
		Vector2_t2156229523  L_39 = V_2;
		float L_40 = ___duration1;
		NullCheck(L_38);
		Block_MoveToPosition_m2829428912(L_38, L_39, L_40, /*hidden argument*/NULL);
		__this->set_blockIsMoving_11((bool)1);
	}

IL_00db:
	{
		return;
	}
}
// System.Void Puzzle::OnBlockFinishedMoving()
extern "C"  void Puzzle_OnBlockFinishedMoving_m4170377730 (Puzzle_t2606799644 * __this, const RuntimeMethod* method)
{
	{
		__this->set_blockIsMoving_11((bool)0);
		Puzzle_CheckIfSolved_m1788021871(__this, /*hidden argument*/NULL);
		int32_t L_0 = __this->get_state_7();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0024;
		}
	}
	{
		Puzzle_MakeNextPlayerMove_m3707527570(__this, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_0024:
	{
		int32_t L_1 = __this->get_state_7();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_2 = __this->get_shuffleMovesRemaining_12();
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0047;
		}
	}
	{
		Puzzle_MakeNextShuffleMove_m3103018245(__this, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_0047:
	{
		__this->set_state_7(2);
	}

IL_004e:
	{
		return;
	}
}
// System.Void Puzzle::StartShuffle()
extern "C"  void Puzzle_StartShuffle_m3525740025 (Puzzle_t2606799644 * __this, const RuntimeMethod* method)
{
	{
		__this->set_state_7(1);
		int32_t L_0 = __this->get_shuffleLength_4();
		__this->set_shuffleMovesRemaining_12(L_0);
		Block_t1429612866 * L_1 = __this->get_emptyBlock_8();
		NullCheck(L_1);
		GameObject_t1113636619 * L_2 = Component_get_gameObject_m442555142(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_SetActive_m796801857(L_2, (bool)0, /*hidden argument*/NULL);
		Puzzle_MakeNextShuffleMove_m3103018245(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Puzzle::MakeNextShuffleMove()
extern "C"  void Puzzle_MakeNextShuffleMove_m3103018245 (Puzzle_t2606799644 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Puzzle_MakeNextShuffleMove_m3103018245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2IntU5BU5D_t2878452246* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector2Int_t3469998543  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2Int_t3469998543  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Vector2IntU5BU5D_t2878452246* L_0 = ((Vector2IntU5BU5D_t2878452246*)SZArrayNew(Vector2IntU5BU5D_t2878452246_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_0);
		Vector2Int_t3469998543  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector2Int__ctor_m3872920888((&L_1), 1, 0, /*hidden argument*/NULL);
		*(Vector2Int_t3469998543 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))) = L_1;
		Vector2IntU5BU5D_t2878452246* L_2 = L_0;
		NullCheck(L_2);
		Vector2Int_t3469998543  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2Int__ctor_m3872920888((&L_3), (-1), 0, /*hidden argument*/NULL);
		*(Vector2Int_t3469998543 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1))) = L_3;
		Vector2IntU5BU5D_t2878452246* L_4 = L_2;
		NullCheck(L_4);
		Vector2Int_t3469998543  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2Int__ctor_m3872920888((&L_5), 0, 1, /*hidden argument*/NULL);
		*(Vector2Int_t3469998543 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2))) = L_5;
		Vector2IntU5BU5D_t2878452246* L_6 = L_4;
		NullCheck(L_6);
		Vector2Int_t3469998543  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector2Int__ctor_m3872920888((&L_7), 0, (-1), /*hidden argument*/NULL);
		*(Vector2Int_t3469998543 *)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(3))) = L_7;
		V_0 = L_6;
		Vector2IntU5BU5D_t2878452246* L_8 = V_0;
		NullCheck(L_8);
		int32_t L_9 = Random_Range_m4054026115(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((RuntimeArray *)L_8)->max_length)))), /*hidden argument*/NULL);
		V_1 = L_9;
		V_2 = 0;
		goto IL_0122;
	}

IL_0064:
	{
		Vector2IntU5BU5D_t2878452246* L_10 = V_0;
		int32_t L_11 = V_1;
		int32_t L_12 = V_2;
		Vector2IntU5BU5D_t2878452246* L_13 = V_0;
		NullCheck(L_13);
		NullCheck(L_10);
		V_3 = (*(Vector2Int_t3469998543 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)L_12))%(int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_13)->max_length))))))))));
		Vector2Int_t3469998543  L_14 = V_3;
		Vector2Int_t3469998543  L_15 = __this->get_prevShuffleOffset_13();
		IL2CPP_RUNTIME_CLASS_INIT(Vector2Int_t3469998543_il2cpp_TypeInfo_var);
		Vector2Int_t3469998543  L_16 = Vector2Int_op_Multiply_m2509535640(NULL /*static, unused*/, L_15, (-1), /*hidden argument*/NULL);
		bool L_17 = Vector2Int_op_Inequality_m156856833(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_011e;
		}
	}
	{
		Block_t1429612866 * L_18 = __this->get_emptyBlock_8();
		NullCheck(L_18);
		Vector2Int_t3469998543  L_19 = L_18->get_coord_4();
		Vector2Int_t3469998543  L_20 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2Int_t3469998543_il2cpp_TypeInfo_var);
		Vector2Int_t3469998543  L_21 = Vector2Int_op_Addition_m1244329832(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		int32_t L_22 = Vector2Int_get_x_m64542184((&V_4), /*hidden argument*/NULL);
		if ((((int32_t)L_22) < ((int32_t)0)))
		{
			goto IL_011e;
		}
	}
	{
		int32_t L_23 = Vector2Int_get_x_m64542184((&V_4), /*hidden argument*/NULL);
		int32_t L_24 = __this->get_blocksPerLine_3();
		if ((((int32_t)L_23) >= ((int32_t)L_24)))
		{
			goto IL_011e;
		}
	}
	{
		int32_t L_25 = Vector2Int_get_y_m64542185((&V_4), /*hidden argument*/NULL);
		if ((((int32_t)L_25) < ((int32_t)0)))
		{
			goto IL_011e;
		}
	}
	{
		int32_t L_26 = Vector2Int_get_y_m64542185((&V_4), /*hidden argument*/NULL);
		int32_t L_27 = __this->get_blocksPerLine_3();
		if ((((int32_t)L_26) >= ((int32_t)L_27)))
		{
			goto IL_011e;
		}
	}
	{
		BlockU5B0___U2C0___U5D_t2917470072* L_28 = __this->get_blocks_9();
		int32_t L_29 = Vector2Int_get_x_m64542184((&V_4), /*hidden argument*/NULL);
		int32_t L_30 = Vector2Int_get_y_m64542185((&V_4), /*hidden argument*/NULL);
		NullCheck((BlockU5B0___U2C0___U5D_t2917470072*)(BlockU5B0___U2C0___U5D_t2917470072*)L_28);
		Block_t1429612866 * L_31 = ((BlockU5B0___U2C0___U5D_t2917470072*)(BlockU5B0___U2C0___U5D_t2917470072*)L_28)->GetAt(L_29, L_30);
		float L_32 = __this->get_shuffleMoveDuration_6();
		Puzzle_MoveBlock_m1974819708(__this, L_31, L_32, /*hidden argument*/NULL);
		int32_t L_33 = __this->get_shuffleMovesRemaining_12();
		__this->set_shuffleMovesRemaining_12(((int32_t)il2cpp_codegen_subtract((int32_t)L_33, (int32_t)1)));
		Vector2Int_t3469998543  L_34 = V_3;
		__this->set_prevShuffleOffset_13(L_34);
		goto IL_012b;
	}

IL_011e:
	{
		int32_t L_35 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
	}

IL_0122:
	{
		int32_t L_36 = V_2;
		Vector2IntU5BU5D_t2878452246* L_37 = V_0;
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray *)L_37)->max_length)))))))
		{
			goto IL_0064;
		}
	}

IL_012b:
	{
		return;
	}
}
// System.Void Puzzle::CheckIfSolved()
extern "C"  void Puzzle_CheckIfSolved_m1788021871 (Puzzle_t2606799644 * __this, const RuntimeMethod* method)
{
	Block_t1429612866 * V_0 = NULL;
	BlockU5B0___U2C0___U5D_t2917470072* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		BlockU5B0___U2C0___U5D_t2917470072* L_0 = __this->get_blocks_9();
		V_1 = (BlockU5B0___U2C0___U5D_t2917470072*)L_0;
		BlockU5B0___U2C0___U5D_t2917470072* L_1 = V_1;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_1);
		int32_t L_2 = Array_GetLength_m2178203778((RuntimeArray *)(RuntimeArray *)L_1, 0, /*hidden argument*/NULL);
		V_2 = L_2;
		BlockU5B0___U2C0___U5D_t2917470072* L_3 = V_1;
		NullCheck((RuntimeArray *)(RuntimeArray *)L_3);
		int32_t L_4 = Array_GetLength_m2178203778((RuntimeArray *)(RuntimeArray *)L_3, 1, /*hidden argument*/NULL);
		V_3 = L_4;
		V_4 = 0;
		goto IL_0052;
	}

IL_001f:
	{
		V_5 = 0;
		goto IL_0044;
	}

IL_0027:
	{
		BlockU5B0___U2C0___U5D_t2917470072* L_5 = V_1;
		int32_t L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_5);
		Block_t1429612866 * L_8 = (L_5)->GetAt(L_6, L_7);
		V_0 = L_8;
		Block_t1429612866 * L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = Block_IsAtStartingCoord_m1016848544(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_003e;
		}
	}
	{
		return;
	}

IL_003e:
	{
		int32_t L_11 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_0044:
	{
		int32_t L_12 = V_5;
		int32_t L_13 = V_3;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_14 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_14, (int32_t)1));
	}

IL_0052:
	{
		int32_t L_15 = V_4;
		int32_t L_16 = V_2;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_001f;
		}
	}
	{
		__this->set_state_7(0);
		Block_t1429612866 * L_17 = __this->get_emptyBlock_8();
		NullCheck(L_17);
		GameObject_t1113636619 * L_18 = Component_get_gameObject_m442555142(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		GameObject_SetActive_m796801857(L_18, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
