﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.UI;

public class LibraryVideoExclusive : MonoBehaviour {
    Button btn_test_download;
    DownloadVideo downloadVideo;
    Image image;
    Button button;
    string platform = "";

    // Use this for initialization
    void Start () {
        downloadVideo = new DownloadVideo();

        btn_test_download = GameObject.Find("btn_test_download").GetComponent<Button>();
        btn_test_download.onClick.AddListener(TestDownload_onClick);

        image = GameObject.Find("Image").GetComponent<Image>();

        button = GameObject.Find("Button").GetComponent<Button>();
        GameObject.Destroy(button.GetComponentInChildren<Text>());

        string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
        filePath = Path.Combine(filePath, "videos");
        filePath = Path.Combine(filePath, "action-anime-wallpaper-11.jpg");
        //button.GetComponent<Image>().mainTexture = filePath;

        GetFiles();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TestDownload_onClick()
    {
        downloadVideo.DownloadFile("http://wallpaper-gallery.net/images/action-anime-wallpaper/action-anime-wallpaper-11.jpg");
    }

    public void GetFiles()
    {
        string folderPath = "";
        
        
        // Set Platform
        if (Application.platform == RuntimePlatform.Android)
        {
            platform = "android";
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            platform = "iphone";
        }
        else if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            platform = "windows";
        }

        // Set folder path
        if (platform == "android")
        {
            folderPath = Application.persistentDataPath + "/videos/";
            if (Directory.Exists(folderPath))
            {
                string[] file_videos = Directory.GetFiles(folderPath);
                foreach(string fileName in file_videos)
                {
                    print(fileName);
                }
            }
        }
        else if (platform == "iphone")
        {
            // Susy help me to set the path to save the videos or file in iOS hehehe. :)
            folderPath = "";
        }
        else if (platform == "windows")
        {
            folderPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            folderPath = Path.Combine(folderPath, "videos");
            if (Directory.Exists(folderPath))
            {
                string[] file_videos = Directory.GetFiles(folderPath);
                foreach (string fileName in file_videos)
                {
                    print(fileName);
                }
            }
        }
    }
}
