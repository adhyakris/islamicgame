﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class World : MonoBehaviour {

    // declaration variable
    Button btn_lvl_1, btn_lvl_2, btn_lvl_3, btn_lvl_4, btn_lvl_5,
        btn_back,
        btn_setting,
        btn_setting_profil, btn_setting_donation, btn_setting_mainmenu, btn_setting_cancel;
    PlayerStats playerStats;
	  LivesEngine livesEngine;
    GameObject[] gameController, gameArena, showOnLifeEmpty, showOnSelectDifficulty, showOnSetting;
    Canvas mainCanvas;
    int int_difficulty;

    // Use this for initialization
    void Start()
    {
        // initialization button level
        btn_lvl_1 = GameObject.Find("btn_lvl_1").GetComponent<Button>();
        btn_lvl_2 = GameObject.Find("btn_lvl_2").GetComponent<Button>();
        btn_lvl_3 = GameObject.Find("btn_lvl_3").GetComponent<Button>();
        btn_lvl_4 = GameObject.Find("btn_lvl_4").GetComponent<Button>();
        btn_lvl_5 = GameObject.Find("btn_lvl_5").GetComponent<Button>();

        // initialization option button
        btn_back = GameObject.Find("btn_back").GetComponent<Button>();
        btn_setting = GameObject.Find("btn_setting").GetComponent<Button>();

        // initialize setting buttons
        btn_setting_profil = GameObject.Find("btn_setting_profil").GetComponent<Button>();
        btn_setting_donation = GameObject.Find("btn_setting_donation").GetComponent<Button>();
        btn_setting_mainmenu = GameObject.Find("btn_setting_mainmenu").GetComponent<Button>();
        btn_setting_cancel = GameObject.Find("btn_setting_cancel").GetComponent<Button>();

        // initialize main canvas
        mainCanvas = GameObject.Find("MainCanvas").GetComponent<Canvas>();

        // initialization playerStatsClass
		    playerStats = gameObject.AddComponent<PlayerStats>();
		    livesEngine = gameObject.AddComponent<LivesEngine>();

        // initialization group of game objects
        gameController = GameObject.FindGameObjectsWithTag("GameController");
        gameArena = GameObject.FindGameObjectsWithTag("gameArea");
        showOnLifeEmpty = GameObject.FindGameObjectsWithTag("ShowOnLifeEmpty");
        showOnSelectDifficulty = GameObject.FindGameObjectsWithTag("ShowOnSelectDifficulty");
        showOnSetting = GameObject.FindGameObjectsWithTag("ShowOnSetting");

        // initialize button onclick
        btn_back.onClick.AddListener(MoveToMainMenu);
        btn_setting.onClick.AddListener(ShowSetting);

        btn_lvl_1.onClick.AddListener(MoveToGameScene);
        btn_lvl_2.onClick.AddListener(MoveToGameScene);
        btn_lvl_3.onClick.AddListener(MoveToGameScene);
        btn_lvl_4.onClick.AddListener(MoveToGameScene);
        btn_lvl_5.onClick.AddListener(MoveToGameScene);

        btn_setting_profil.onClick.AddListener(MoveToProfileScene);
        btn_setting_donation.onClick.AddListener(MoveToDonationScene);
        btn_setting_mainmenu.onClick.AddListener(MoveToMainMenu);
        btn_setting_cancel.onClick.AddListener(HideSetting);

        // initialize difficulty
        int_difficulty = playerStats.GetDifficulty();

        HideSetting();
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    ///     throw to main menu (quit for now because there is no main menu scene)
    /// </summary>
    void MoveToMainMenu()
    {
        Debug.Log("Move to Main Menu");
        if (int_difficulty != 0)
        {
            // back to select level scene
            SceneManager.LoadScene("SelectLevelSP");
        }
        else
        {
            // quit game
            Application.Quit();
            //
            // Set scene manager to main menu
            //SceneManager.LoadScene();
        }
    }

    void MoveToGameScene()
    {
        SceneManager.LoadScene("Level1Scene");
    }

    void HideSetting()
    {
        foreach(GameObject g in showOnSetting)
        {
            g.SetActive(false);
        }
    }

    void ShowSetting()
    {
        foreach (GameObject g in showOnSetting)
        {
            g.SetActive(true);
        }
    }

    void MoveToProfileScene()
    {
        Debug.Log("Profile button pressed");
        //SceneManager.LoadScene("ProfileScene");
    }

    void MoveToDonationScene()
    {
        Debug.Log("Donation Button pressed");
        //SceneManager.LoadScene("DonationScene");
    }
}
