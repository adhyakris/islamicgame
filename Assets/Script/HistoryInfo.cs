﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net;
using System.IO;

public class HistoryInfo : MonoBehaviour {
    string platform;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void getPlatfrom()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            platform = "android";
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            platform = "iphone";
        }
        else if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            platform = "windows";
        }
    }

    public void DownloadFile(string input_url)
    {
        string filePath = "";
        string filename;
        Uri uri;
        WebClient client;

        platform = "";


        // get platform
        if (platform == "" || platform == null)
        {
            getPlatfrom();
        }


        // Get Filename
        uri = new Uri(input_url);
        filename = System.IO.Path.GetFileName(uri.LocalPath);


        // Set saving path
		if (platform == "android" || platform == "iphone")
        {
            string folderPath = Application.persistentDataPath + "/images/";
            if (!Directory.Exists(folderPath))
            {
                System.IO.Directory.CreateDirectory(folderPath);
            }
            filePath = folderPath + filename;
			//FileStream file = File.Create(Application.persistentDataPath + "/images/");
        }
        else if (platform == "windows")
        {
            filePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            filePath = Path.Combine(filePath, "images");
            if (!Directory.Exists(filePath))
            {
                System.IO.Directory.CreateDirectory(filePath);
            }
            filePath = Path.Combine(filePath, filename);
        }

        if (!File.Exists(filePath))
        {
            // Download the file if file not exsist
            client = new WebClient();
            client.DownloadFile(input_url, filePath);
        }
        else
        {
            print("File Exsist");
        }
    }
}
