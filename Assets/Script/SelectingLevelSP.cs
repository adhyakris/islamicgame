﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectingLevelSP : MonoBehaviour {

	Button btn_easy, btn_med, btn_hard;
    int life;
    PlayerStats playerStats;
    LivesEngine livesEngine;
    bool isHardEnable;

	void Start(){
		playerStats = gameObject.AddComponent<PlayerStats>();
        livesEngine = gameObject.AddComponent<LivesEngine>();

		btn_easy = GameObject.Find ("btn_lvl_easy").GetComponent<Button> ();
		btn_med = GameObject.Find ("btn_lvl_med").GetComponent<Button> ();
		btn_hard = GameObject.Find ("btn_lvl_hard").GetComponent<Button> ();

		btn_easy.onClick.AddListener (btn_easy_onClick);
		btn_med.onClick.AddListener (btn_med_onClick);
		btn_hard.onClick.AddListener (btn_hard_onClick);

        life = playerStats.GetLife();
        print("life = " + life);
        isHardEnable = playerStats.GetLevelHard();
        if (isHardEnable)
        {
            btn_hard.interactable = true;
        }
        else
        {
            btn_hard.interactable = false;
        }
	}

	void Quit(){
		Application.Quit();
	}

	void btn_easy_onClick(){
        if(life != 0)
        {
            playerStats.SetDifficulty(1);
            SceneManager.LoadScene("Level1Scene");
        }
	}

	void btn_med_onClick(){
        if (life != 0)
        {
            playerStats.SetDifficulty(2);
            SceneManager.LoadScene("Level1Scene");
        }
    }

	void btn_hard_onClick(){
        if (life != 0)
        {
            playerStats.SetDifficulty(3);
            SceneManager.LoadScene("Level1Scene");
        }
    }

}
