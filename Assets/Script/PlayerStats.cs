﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour {
  private static int life = 5, int_difficulty, point, donation;
	private static bool first_start, level_hard = false;

    //===============================================
    //SETTER
    //===============================================
    public void SetLife(int newLife)
    {
        life = newLife;
    }

    public void SetDifficulty(int newDifficulty)
    {
        int_difficulty = newDifficulty;
    }

    public void SetPoint(int newPoint)
    {
        point = newPoint;
    }

    public void SetDonation(int newDonation)
    {
        donation = newDonation;
    }

	public void SetFirstStart(bool isFirstStart)
	{
		first_start = isFirstStart;
	}

    public void SetLevelHard(bool levelHard)
    {
        level_hard = levelHard;
    }
    //===============================================

    //===============================================
    //GETTER
    //===============================================
    public int GetLife()
    {
        return life;
    }

    public int GetDifficulty()
    {
        return int_difficulty;
    }

    public int GetPoint()
    {
        return point;
    }

    public int GetDonation()
    {
        return donation;
    }

	public bool GetFirstStart(){
		return first_start;
	}

    public bool GetLevelHard()
    {
        return level_hard;
    }
    //===============================================

}
