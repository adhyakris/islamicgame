﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Puzzle : MonoBehaviour {

    public Texture2D image;
    private int blocksPerLine = 4;
    private int shuffleLength = 5;
    public float defaultMoveDuration = .2f;
    public float shuffleMoveDuration = .1f;

    enum PuzzleState { Solved, Shuffling, InPlay };
    PuzzleState state;

    Block emptyBlock;
    Block[,] blocks;
    Queue<Block> inputs;
    bool blockIsMoving;
    int shuffleMovesRemaining;
    Vector2Int prevShuffleOffset;

    // additional variables
    bool shufle = false, start_countdown = false, gamePaused = false, gameoverFlag = false;
    int time, levelSelected = 0;
    float float_time;
    int life;

    Button btn_history, btn_next, btn_previous, btn_pause, 
        btn_pause_resume, btn_pause_restart, btn_pause_mainmenu,
        btn_gameover_restart, btn_gameover_mainmenu,
        btn_complete_restart, btn_complete_mainmenu, btn_complete_next, btn_complete_history;
    Text lbl_secondsleft, lbl_time, lbl_life;
    PlayerStats playerStats;
	GameObject[] pauseObjects, gameAreaObjects, gameOverObjects, gameCompleteObjects, lifeEmptyObjects;

    // additional variables - Empty life
    Text lbl_lifeempty_liferemaining;
    Button btn_lifeempty_watchvideo, btn_lifeempty_buylife, btn_lifeempty_mainmenu;

	LivesEngine livesEngine;

    void Start()
    {
        Time.timeScale = 1;
		livesEngine = gameObject.AddComponent<LivesEngine>();
		playerStats = gameObject.AddComponent<PlayerStats>();
        int difficulty = playerStats.GetDifficulty();

        // button and text in game
        btn_history     = GameObject.Find("btn_history").GetComponent<Button>();
        btn_next        = GameObject.Find("btn_next").GetComponent<Button>();
        btn_previous    = GameObject.Find("btn_prev").GetComponent<Button>();
        lbl_secondsleft = GameObject.Find("lbl_secondsleft").GetComponent<Text>();
        lbl_time        = GameObject.Find("lbl_time").GetComponent<Text>();
		lbl_life        = GameObject.Find("lbl_life").GetComponent<Text>();
		btn_pause       = GameObject.Find("btn_pause").GetComponent<Button>();

        // button in pause panel
        btn_pause_resume   = GameObject.Find("btn_pause_resume").GetComponent<Button>();
        btn_pause_restart  = GameObject.Find("btn_pause_restart").GetComponent<Button>();
        btn_pause_mainmenu = GameObject.Find("btn_pause_mainmenu").GetComponent<Button>();

        // button in game over panel
        btn_gameover_restart  = GameObject.Find("btn_gameover_restart").GetComponent<Button>();
        btn_gameover_mainmenu = GameObject.Find("btn_gameover_mainmenu").GetComponent<Button>();

        // button in complete panel
        btn_complete_restart = GameObject.Find("btn_complete_restart").GetComponent<Button>();
        btn_complete_mainmenu = GameObject.Find("btn_complete_mainmenu").GetComponent<Button>();
        btn_complete_next = GameObject.Find("btn_complete_next").GetComponent<Button>();
        btn_complete_history = GameObject.Find("btn_complete_history").GetComponent<Button>();

        // button and text in life empty panel
        btn_lifeempty_watchvideo = GameObject.Find("btn_lifeempty_watchvideo").GetComponent<Button>();
        btn_lifeempty_buylife = GameObject.Find("btn_lifeempty_buylife").GetComponent<Button>();
        btn_lifeempty_mainmenu = GameObject.Find("btn_lifeempty_mainmenu").GetComponent<Button>();

        // button onClick
        btn_history.onClick.AddListener(ShowHistory);
        btn_next.onClick.AddListener(NextLevel);
        btn_previous.onClick.AddListener(btn_previous_onClick);
		btn_pause.onClick.AddListener(btn_pause_onClick);

        // button onClick in pause panel
		btn_pause_resume.onClick.AddListener(btn_resume_onClick);
        btn_pause_restart.onClick.AddListener(RestartLevel);
        btn_pause_mainmenu.onClick.AddListener(MainMenu);

        // button onClick in game over panel
        btn_gameover_restart.onClick.AddListener(RestartLevel);
        btn_gameover_mainmenu.onClick.AddListener(MainMenu);

        // button onClick in complete panel
        btn_complete_restart.onClick.AddListener(RestartLevel);
        btn_complete_mainmenu.onClick.AddListener(MainMenu);
        btn_complete_next.onClick.AddListener(NextLevel);
        btn_complete_history.onClick.AddListener(ShowHistory);

        // button onClick in life empty panel
        btn_lifeempty_watchvideo.onClick.AddListener(WatchAdsVideo);
        btn_lifeempty_buylife.onClick.AddListener(BuyLife);
        btn_lifeempty_mainmenu.onClick.AddListener(MainMenu);

        // initialize Objects with tag
        pauseObjects        = GameObject.FindGameObjectsWithTag("ShowOnPause");
		gameAreaObjects     = GameObject.FindGameObjectsWithTag("gameArea");
        gameOverObjects     = GameObject.FindGameObjectsWithTag("ShowOnGameOver");
        gameCompleteObjects = GameObject.FindGameObjectsWithTag("ShowOnGameComplete");
        lifeEmptyObjects    = GameObject.FindGameObjectsWithTag("ShowOnLifeEmpty");

        // Do some thing to hide objects
        HidePaused();
        HideGameOver();
        HideGameComplete();
        HideLifeEmpty();

        btn_next.gameObject.SetActive(false);
        btn_previous.gameObject.SetActive(false);
        btn_history.gameObject.SetActive(false);

        // Disable history button and next button while start game
        btn_history.interactable = false; // Make the btn_history disable
        btn_next.interactable = false; // Make the btn_next disable

        // Set the difficulty
        switch (difficulty)
        {
		    case 1: // easy
			    blocksPerLine = 3;
			    shuffleLength = 30;
			    time = 300;
                break;
            case 2: // medium
                blocksPerLine = 4;
                shuffleLength = 40;
                time = 600;
                break;
            case 3: // hard
                blocksPerLine = 5;
                shuffleLength = 50;
                time = 900;
                break;
            case 4: // super hard, no time limit
                blocksPerLine = 6;
                shuffleLength = 60;
                time = 8888;
                break;
            default:
                blocksPerLine = 4;
                //shuffleLength = 40;
                //time = 600;
                shuffleLength = 2;
                time = 5;
                break;
        }

        if (time != 8888)
        {
            lbl_time.text = "Waktu :";
            lbl_secondsleft.text = time.ToString();

        }
        else
        {
            lbl_secondsleft.text = "";
            lbl_time.text = "";
        }

        float_time = (float)time;

        // show life on game
        lbl_life.text = "Life : " + playerStats.GetLife();
        life = playerStats.GetLife();

        // Start create puzzle
        CreatePuzzle();
    }

    void Update()
    {
		life = playerStats.GetLife();
        lbl_life.text = "Life : " + life;
	
        if (state == PuzzleState.Solved && shufle == false)
        {
            StartShuffle();
            shufle = true;
        }

        if (start_countdown)
        {
            // If timeup do something
            if (float_time < 1)
            {
                //print("Game Over");
                //foreach(GameObject g in gameAreaObjects){
                //	g.SetActive(false);
                //}
                if (gameoverFlag == false)
                {
                    // decrease user life
                    playerStats.SetLife(playerStats.GetLife() - 1);
                    life = life - 1;
                    lbl_life.text = "Life : " + life;

                    // Set gameobjects in gameOverObjects active
                    foreach (GameObject g in gameOverObjects)
                    {
                        g.SetActive(true);
                    }

                    HideGameArena();
                    Time.timeScale = 1;

                    // set btn_pause disabled
                    btn_pause.interactable = false;

                    //set flag true
                    gameoverFlag = true;

                    // show life on game

                }
            }
            else
            {
                // Timer countdown
                float_time -= Time.deltaTime;
                lbl_secondsleft.text = ((int)float_time).ToString();
            }
        }

        if(Application.platform != RuntimePlatform.IPhonePlayer)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                //MainMenu();
                PauseControl();
            }
        }
    }

	void CreatePuzzle()
    {
        blocks = new Block[blocksPerLine, blocksPerLine];
        Texture2D[,] imageSlices = ImageSlicer.GetSlices(image, blocksPerLine);
        for (int y = 0; y < blocksPerLine; y++)
        {
            for (int x = 0; x < blocksPerLine; x++)
            {
                GameObject blockObject = GameObject.CreatePrimitive(PrimitiveType.Quad);
                blockObject.transform.position = -Vector2.one * (blocksPerLine - 1) * .5f + new Vector2(x, y);
                blockObject.transform.parent = transform;

                Block block = blockObject.AddComponent<Block>();
                block.OnBlockPressed += PlayerMoveBlockInput;
                block.OnFinishedMoving += OnBlockFinishedMoving;
                block.Init(new Vector2Int(x, y), imageSlices[x, y]);
                blocks[x, y] = block;

                if (y == 0 && x == blocksPerLine - 1)
                {
                    emptyBlock = block;
                }
            }
        }

        Camera.main.orthographicSize = blocksPerLine * .90f;
        //Camera.main.orthographicSize = 5 * .90f;
        inputs = new Queue<Block>();
    }

    void PlayerMoveBlockInput(Block blockToMove)
    {
        if (state == PuzzleState.InPlay)
        {
            inputs.Enqueue(blockToMove);
            MakeNextPlayerMove();
            start_countdown = true;
        }
    }

    void MakeNextPlayerMove()
    {
		while (inputs.Count > 0 && !blockIsMoving)
		{
            MoveBlock(inputs.Dequeue(), defaultMoveDuration);
		}
    }

    void MoveBlock(Block blockToMove, float duration)
    {
		if ((blockToMove.coord - emptyBlock.coord).sqrMagnitude == 1)
		{
            blocks[blockToMove.coord.x, blockToMove.coord.y] = emptyBlock;
            blocks[emptyBlock.coord.x, emptyBlock.coord.y] = blockToMove;

			Vector2Int targetCoord = emptyBlock.coord;
			emptyBlock.coord = blockToMove.coord;
			blockToMove.coord = targetCoord;

			Vector2 targetPosition = emptyBlock.transform.position;
			emptyBlock.transform.position = blockToMove.transform.position;
            blockToMove.MoveToPosition(targetPosition, duration);
            blockIsMoving = true;
		}
    }

    void OnBlockFinishedMoving()
    {
        blockIsMoving = false;
        CheckIfSolved();

        if (state == PuzzleState.InPlay)
        {
            MakeNextPlayerMove();
        }
        else if (state == PuzzleState.Shuffling)
        {
            if (shuffleMovesRemaining > 0)
            {
                MakeNextShuffleMove();
            }
            else
            {
                state = PuzzleState.InPlay;
            }
        }
    }

    void StartShuffle()
    {
        state = PuzzleState.Shuffling;
        shuffleMovesRemaining = shuffleLength;
        emptyBlock.gameObject.SetActive(false);
        MakeNextShuffleMove();
    }

    void MakeNextShuffleMove()
    {
        Vector2Int[] offsets = { new Vector2Int(1, 0), new Vector2Int(-1, 0), new Vector2Int(0, 1), new Vector2Int(0, -1) };
        int randomIndex = Random.Range(0, offsets.Length);

        for (int i = 0; i < offsets.Length; i++)
        {
            Vector2Int offset = offsets[(randomIndex + i) % offsets.Length];
            if (offset != prevShuffleOffset * -1)
            {
                Vector2Int moveBlockCoord = emptyBlock.coord + offset;

                if (moveBlockCoord.x >= 0 && moveBlockCoord.x < blocksPerLine && moveBlockCoord.y >= 0 && moveBlockCoord.y < blocksPerLine)
                {
                    MoveBlock(blocks[moveBlockCoord.x, moveBlockCoord.y], shuffleMoveDuration);
                    shuffleMovesRemaining--;
                    prevShuffleOffset = offset;
                    break;
                }
            }
        }
      
    }

    void CheckIfSolved()
    {
        foreach (Block block in blocks)
        {
            if (!block.IsAtStartingCoord())
            {
                return;
            }
        }

        state = PuzzleState.Solved;
        emptyBlock.gameObject.SetActive(true);
        btn_next.interactable = true;
        btn_history.interactable = true;
        start_countdown = false;
        ShowGameComplete();
        HideGameArena();
    }

    // ==================================================
    // Button Function
    // ==================================================
    /// <summary>
    ///     This function is used to control what happen if btn_history clicked
    /// </summary>
    void ShowHistory()
    {
        print("History Button");
    }

    /// <summary>
    ///     Move to next level
    /// </summary>
    void NextLevel()
    {
        print("Next Button");
    }

    /// <summary>
    ///     This function is used to do something when btn_previous clicked
    /// </summary>
    void btn_previous_onClick()
    {
        SceneManager.LoadScene("SelectLevelSP");
    }

	void btn_pause_onClick()
	{
		gamePaused = true;
        PauseControl();
	}

	void btn_resume_onClick()
	{
		gamePaused = false;
        PauseControl();
	}

    /// <summary>
    ///     Restart this level
    /// </summary>
	void RestartLevel()
	{
        if(life > 0)
        {
            foreach (GameObject g in gameAreaObjects)
            {
                g.SetActive(true);
            }
            btn_pause.interactable = true;
            SceneManager.LoadScene("Level1Scene");
        }else
        {
            print("hide paused");
            HidePaused();

            print("hide game over");
            HideGameOver();

            print("hide game complete");
            HideGameComplete();
            btn_pause.interactable = false;

            print("show life empty");
            ShowLifeEmpty();
        }
    }

    /// <summary>
    ///     Move to Main Menu Scene
    /// </summary>
	void MainMenu()
	{
		SceneManager.LoadScene("SelectLevelSP");
	}

    /// <summary>
    ///     Buy life using google play (In App Purchase)
    /// </summary>
    void BuyLife()
    {

    }

    /// <summary>
    ///     Watchs ads videos
    /// </summary>
    void WatchAdsVideo()
    {

    }
    
    // ==================================================

    // controls the pausing of the scene
    public void PauseControl(){
		if(Time.timeScale == 1)
		{
			Time.timeScale = 0;
            btn_pause.interactable = true;
            ShowPaused();
            HideGameArena();
		} else if (Time.timeScale == 0){
			Time.timeScale = 1;
            btn_pause.interactable = false;
            HidePaused();
            ShowGameArena();
		}
	}

	// shows objects with ShowOnPause tag
	public void ShowPaused(){
		foreach(GameObject g in pauseObjects){
			g.SetActive(true);
		}
        btn_pause.interactable = false;
    }

	// hides objects with ShowOnPause tag
	public void HidePaused(){
		foreach(GameObject g in pauseObjects){
			g.SetActive(false);
		}
        btn_pause.interactable = true;
    }

    // shows objects with ShowOnGameOver tag
    public void ShowGameOver()
    {
        foreach (GameObject g in gameOverObjects)
        {
            g.SetActive(true);
        }
    }

    // hides objects with ShowOnGameOver tag
    public void HideGameOver()
    {
        foreach (GameObject g in gameOverObjects)
        {
            g.SetActive(false);
        }
    }

    // shows objects with ShowOnGameComplete tag
    public void ShowGameComplete()
    {
        foreach (GameObject g in gameCompleteObjects)
        {
            g.SetActive(true);
            btn_pause.interactable = false;
        }
    }

    // hides objects with ShowOnGameComplete tag
    public void HideGameComplete()
    {
        foreach (GameObject g in gameCompleteObjects)
        {
            g.SetActive(false);
        }
        btn_pause.interactable = true;
    }

    // shows objects with ShowOnGameComplete tag
    public void ShowLifeEmpty()
    {
        foreach (GameObject g in lifeEmptyObjects)
        {
            g.SetActive(true);
        }
        btn_pause.interactable = false;
    }

    // hides objects with ShowOnGameComplete tag
    public void HideLifeEmpty()
    {
        foreach (GameObject g in lifeEmptyObjects)
        {
            g.SetActive(false);
        }
        btn_pause.interactable = true;
    }

    // show gameArena tag
    public void ShowGameArena()
    {
        foreach (GameObject g in gameAreaObjects)
        {
            g.SetActive(true);
        }
        Time.timeScale = 1;
    }

    // hide gameArena tag
    public void HideGameArena()
    {
        foreach(GameObject g in gameAreaObjects)
        {
            g.SetActive(false);
        }
        Time.timeScale = 0;
    }
}
