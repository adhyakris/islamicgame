﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net.Sockets;
using System.Net;

public class LivesEngine : MonoBehaviour {

	private static DateTime? energyRefillTime = null;
	PlayerStats playerStats;
	string platform;

	private const int MAX_LIVES = 5;

	// 5 seconds for testing
	private static TimeSpan newLifeInterval = new TimeSpan(0,0,5);

	private int currentLife = 0;
	private int amountOfIntervalsPassed;

	private bool isWaitingForUpdateLife = false;

	private DateTime lostLifeTimeStamp;
	private int livesLeft = 0;
	private int lastTime = 0;
	private int currentSeconds = 0;


	void Start(){
		playerStats = gameObject.AddComponent<PlayerStats>();
		lastTime = PlayerPrefs.GetInt("LastHeartIncreaseTime");

		currentLife = playerStats.GetLife();
        if(currentLife > MAX_LIVES){
            playerStats.SetLife(5);
        }

		if (currentLife < MAX_LIVES && isWaitingForUpdateLife == false)
		{
			isWaitingForUpdateLife = true;
			PlayerPrefs.SetInt("LastHeartIncreaseTime", (int)ConvertToUnixTimestamp(DateTime.Now));

		}

		currentSeconds = (int)ConvertToUnixTimestamp(DateTime.Now);
		lastTime = PlayerPrefs.GetInt("LastHeartIncreaseTime");

        if ((currentSeconds - lastTime >= (60 * 5)) && isWaitingForUpdateLife == true ) { // diff in seconds
			playerStats.SetLife (playerStats.GetLife() + 1);
			isWaitingForUpdateLife = false;
			Debug.Log ("[Live Engine]Life Updated, current life " + playerStats.GetLife());
		}
        Debug.Log("[Live Engine]current life " + playerStats.GetLife());
	}
	// Update is called once per frame
	void Update () 
	{
        currentLife = playerStats.GetLife();
        if (currentLife > MAX_LIVES)
        {
            playerStats.SetLife(5);
        }

        if(playerStats.GetLife() == 5){
            if (Application.platform == RuntimePlatform.Android){
                  // Herman, please help to set notification in android. Thanks :)

              } else if (Application.platform == RuntimePlatform.IPhonePlayer) {
                  UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications ();
                  UnityEngine.iOS.NotificationServices.ClearLocalNotifications ();

                  if (energyRefillTime.HasValue) {
                      UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification ();
                      notif.fireDate = energyRefillTime.Value;
                      notif.alertBody = "You have full energy!";
                      notif.alertAction = "The wait is over!";
                      notif.soundName = UnityEngine.iOS.LocalNotification.defaultSoundName;
                      notif.applicationIconBadgeNumber = 1;
                      UnityEngine.iOS.NotificationServices.ScheduleLocalNotification (notif);

                  }
              } else {
                  // windows
              }
        }

		if (currentLife < MAX_LIVES && isWaitingForUpdateLife == false)
		{
			isWaitingForUpdateLife = true;
            PlayerPrefs.SetInt("LastHeartIncreaseTime", (int)ConvertToUnixTimestamp(DateTime.Now));

		}

        currentSeconds = (int)ConvertToUnixTimestamp(DateTime.Now);
        lastTime = PlayerPrefs.GetInt("LastHeartIncreaseTime");

        if ((currentSeconds-lastTime >= (60 * 5)) && isWaitingForUpdateLife == true ) { // diff in seconds
			playerStats.SetLife (playerStats.GetLife() + 1);
			isWaitingForUpdateLife = false;
			Debug.Log ("[Live Engine]Life Updated, current life " + playerStats.GetLife());
		}
        Debug.Log("[Live Engine]current life " + playerStats.GetLife());
   	}

    public static DateTime ConvertFromUnixTimestamp(double timestamp)
    {
        DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        return origin.AddSeconds(timestamp);
    }

    public static double ConvertToUnixTimestamp(DateTime date)
    {
        DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        TimeSpan diff = date.ToUniversalTime() - origin;
        return Math.Floor(diff.TotalSeconds);
    }	

//	void OnApplicationPause(bool loseFocus) {
//		if (loseFocus) {
//			SetAllNotifications();
//			return;
//		}
//
//		// gained focus!
//		CheckIfEnergyWaitTimeOver();
//	}
//
//	private void SetAllNotifications() {
		
//		if (Application.platform == RuntimePlatform.Android) {
//			// Herman, please help to set notification in android. Thanks :)
//
//		} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
//			UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications ();
//			UnityEngine.iOS.NotificationServices.ClearLocalNotifications ();
//
//			if (energyRefillTime.HasValue) {
//				UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification ();
//				notif.fireDate = energyRefillTime.Value;
//				notif.alertBody = "You have more energy!";
//				notif.alertAction = "The wait is over!";
//				notif.soundName = UnityEngine.iOS.LocalNotification.defaultSoundName;
//				notif.applicationIconBadgeNumber = 1;
//				UnityEngine.iOS.NotificationServices.ScheduleLocalNotification (notif);
//			}
//		} else {
//			// windows
//		}
//	}
//
//	public bool CheckIfEnergyWaitTimeOver() {
//		if (!energyRefillTime.HasValue || DateTime.Now < energyRefillTime.Value) {
//			if (energyRefillTime.HasValue) {
//				TimeSpan remaining = energyRefillTime.Value - DateTime.Now;
//				var timerCountdownText = string.Format("{0:D2}:{1:D2}", remaining.Minutes, remaining.Seconds);
//				// use timerCountdownText and display it somewhere!
//
//				return false;
//			} else {
//				// no refill time set!
//			}
//
//			return true;
//		}
//
//		RemoveNotification();
//		return true;
//	}
//
////	public static void RemoveNotification() {
//	public void RemoveNotification() {
//		if (Application.platform == RuntimePlatform.Android) {
//			// Herman, please help to clear notificaion badges in android. Thanks :)
//
//		} else if (Application.platform == RuntimePlatform.IPhonePlayer) {
//			UnityEngine.iOS.LocalNotification notif = new UnityEngine.iOS.LocalNotification ();
//			notif.applicationIconBadgeNumber = 0;
//			//			UnityEngine.iOS.NotificationHelper.ClearAllBadges();
//		} else {
//			//windows
//		}
//
//		// Add code to give player more energy!
//		playerStats.SetLife(playerStats.GetLife() + 1);
//
//		energyRefillTime = null;
//	}

}